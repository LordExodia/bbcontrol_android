package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_promotion.*

class PromotionActivity : AppCompatActivity() {

    var promotions = arrayListOf<DevPromotion>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promotion)

        cargarPromotions()

        btnWaiter.setOnClickListener {
            val intent = Intent(this, AdminActivity::class.java)
            finish()
            startActivity(intent)
        }

        btnAdd.setOnClickListener {
            val intent = Intent(this, CreatePromotionActivity::class.java)
            finish()
            startActivity(intent)
        }
        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.daily_promotions)
        //------------------------------------------------------------------------
    }

    private fun cargarPromotions() {
        val path = "Promotions"

        db.collection(path)
            .get()
            .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    var cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data)
                        var str = ""
                        var datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str =str.plus(dat.toString()).plus("|")
                        }
                        var parts = str.split("|")
                        println(parts)
                        if(parts[2].toString() == "true" ||parts[2].toString() == "false" ){
                            var promo = DevPromotion(
                                parts[1],
                                parts[3],
                                parts[0].toInt(),
                                parts[2].toBoolean(),
                                parts[4].toString()
                            )
                            promotions.add(promo)
                        }
                        else {
                            var promo = DevPromotion(
                                parts[1],
                                parts[2],
                                parts[0].toInt(),
                                parts[3].toBoolean(),
                                parts[4].toString()
                            )
                            promotions.add(promo)
                            //println(promo)
                        }


                        if(promotions.size == cont)
                        {
                            cargar()
                        }
                    }

                    Log.d("DB","SE OBTIENE TODA LA INFORMACION DE PROMOS")
                }
            }
    }

    fun cargar() {

        var llPromotion = findViewById<LinearLayout>(R.id.llPromotion)
        val lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0

        for(promo in promotions) {

            var txt1 = TextView(this)
            txt1.id = cont + 200
            txt1.layoutParams = lp2
            txt1.text = promo.name
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.id = cont + 400
            txt2.layoutParams = lp2
            if (promo.active){
                txt2.text = getString(R.string.Active)
                txt2.setTextColor(Color.GREEN)
            }
            else{
                txt2.text = getString(R.string.Inactive)
                txt2.setTextColor(Color.RED)
            }

            var chBox = CheckBox(this)
            chBox.id = cont + 600
            chBox.layoutParams = lp2
            chBox.isChecked = promo.active

            chBox.setOnClickListener {
                var userRef = db.collection("Promotions").document(promo.id)
                userRef.update("active",chBox.isChecked)
                    .addOnSuccessListener {
                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                        val intent = Intent(this, PromotionActivity::class.java)
                        finish()
                        startActivity(intent)
                    }
            }

            val card = CardView(this)
            card.id = cont + 600
            cont+=1
            card.layoutParams = lp2
            card.elevation = 10f
            card.useCompatPadding = true

            // Create RelativeLayout
            val relative = RelativeLayout(this)
            val lp: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            lp.setMargins(10,10,10,0)

            val paramsState: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsState.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsState.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsState.setMargins(10,10,0,0)

            val paramsDate: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsDate.addRule(RelativeLayout.BELOW, txt1.id)
            paramsDate.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsDate.setMargins(10,10,0,0)

            val paramsPeople: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_END)
            paramsPeople.setMargins(0,10,20,0)

            relative.addView(txt1, paramsState)
            relative.addView(txt2, paramsDate)
            relative.addView(chBox, paramsPeople)

            card.addView(relative)

            llPromotion.addView(card)
        }
    }
}