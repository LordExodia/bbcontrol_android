package com.example.bbcontrol

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_registerctivity.*
import java.time.LocalTime
import java.util.*

class Registerctivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    // Access a Cloud Firestore instance from your Activity

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registerctivity)

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.register_activity)
        //------------------------------------------------------------------------

        auth = FirebaseAuth.getInstance()
        val db = FirebaseFirestore.getInstance()

        btnRegister.setOnClickListener {
            val online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                val time = LocalTime.now().toString()
                val tot = etDate.text.toString().plus(" ").plus(time).plus(" UTC-5")

                auth.createUserWithEmailAndPassword(etMail.text.toString(), etPassword.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success")
                            auth.currentUser
                            println(etPhone.text.toString())

                            val creat = hashMapOf(
                                "birthDate" to tot,
                                "email" to etMail.text.toString(),
                                "firstName" to etName.text.toString(),
                                "lastName" to etLast.text.toString(),
                                "limitAmount" to 0,
                                "phoneNumber" to etPhone.text.toString().toDouble()
                            )
                            println(creat)
                            db.collection("Customers")
                                .add(creat)
                                .addOnSuccessListener { documentReference ->
                                    val usuario = db.collection("Customers").document(documentReference.id)
                                    usuario
                                        .update("id", documentReference.id)
                                        .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!") }
                                        .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
                                    Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")

                                    val intent = Intent(this, MainActivity::class.java)
                                    intent.putExtra("Current", etMail.text.toString())
                                    intent.putExtra("Order","")
                                    startActivity(intent)
                                }
                                .addOnFailureListener { e ->
                                    Log.w(TAG, "Error adding document", e)
                                }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.exception)

                            val snackbar = Snackbar.make(register,
                                "You cant enter email already in use",
                                Snackbar.LENGTH_INDEFINITE)

                            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                            snackbar.setAction("Ok") {
                                snackbar.dismiss()
                            }
                            snackbar.show()
                        }
                    }
            }
            else{
                val snackbar = Snackbar.make(register,
                    "No internet connection to any Network",
                    Snackbar.LENGTH_INDEFINITE)

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
    }
    companion object{
        private const val TAG = "EmailPasswordSIGNUP"
    }

    fun setDate(view: View) {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dateSpinner = DatePickerDialog(this, DatePickerDialog.OnDateSetListener
        { datePicker, year, monthOfYear, dayOfMonth ->
            etDate.setText("$dayOfMonth/$monthOfYear/$year")
        }, year-18, month, day)
        dateSpinner.datePicker.maxDate = System.currentTimeMillis() - 18L*365*24*60*60*1001
        dateSpinner.show()
    }
}
