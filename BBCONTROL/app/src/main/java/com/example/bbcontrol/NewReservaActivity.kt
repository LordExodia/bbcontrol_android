package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.core.view.marginLeft
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.card.MaterialCardView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_new_reserva.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NewReservaActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    private val reservas =arrayListOf<DevReservation>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_reserva)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_new_reserva)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_new_reserva)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_new_reserva)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_new_reserva, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_new_reserva)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_new_reserva)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_new_reserva, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.new_reserva_activity)
        //------------------------------------------------------------------------

        val sharedPref = getSharedPreferences("USERINFO", 0)
        val loguser = sharedPref.getString("UserId","")
        //Log.d("USER",loguser)
        checkReservation(loguser)

        btnGoReserve.setOnClickListener {
            val intent = Intent(this, TableActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun checkReservation(loguser: String?) {
        val path = "Reservations"
        db.collection(path)
            .whereEqualTo("user_Id", loguser)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data?.values.toString())
                        var str = ""
                        for (dat in document.data?.values!!){
                            str = str.plus(dat.toString()).plus("|")

                        }
                        println(str)
                        val parts = str.split("|")
                        val pref = parts[1].split("[")[1].split("]")[0].split(",")
                        val user = parts[2]
                        val cantidad = parts[6]
                        val id = parts[5]
                        val pdate1 = parts[0].split("(")[1].split(")")[0].split(",")
                        val pdate2 = parts[3].split("(")[1].split(")")[0].split(",")
                        val pdate3 = parts[4].split("(")[1].split(")")[0].split(",")
                        val d1 = Timestamp(pdate1[0].split("=")[1].toLong(),pdate1[1].split("=")[1].toInt())
                        val d2 = Timestamp(pdate2[0].split("=")[1].toLong(),pdate2[1].split("=")[1].toInt())
                        val d3 = Timestamp(pdate3[0].split("=")[1].toLong(),pdate3[1].split("=")[1].toInt())
                        val reser = DevReservation(d1,d3,id,cantidad.toInt(),ArrayList(pref),d2,user)
                        println(reser)
                        reservas.add(reser)
                        /*Log.d("DB","Se OBTIENE TODA LA INFORMACION")*/
                    }
                    cargar()
                }
            }
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun cargar() {

        val llFood = findViewById<LinearLayout>(R.id.llReservations)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)
        val lp3 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0
        for(p in reservas) {
            val card = CardView(this)
            card.id = cont
            cont+=1
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val n = Timestamp.now()
            //-------------- Status ----------------
            val txt1 = TextView(this)
            txt1.layoutParams = lp2
            txt1.id = cont
            var estado: String
            if (n<p.end){
                estado= "Active Reservation"
                txt1.text = estado
                txt1.setTextColor(Color.GREEN)
            }
            else{
                estado= "Finalized Reservation"
                txt1.text = estado
                txt1.setTextColor(Color.RED)
            }
            txt1.textSize = 20f

            //-------------- Num of people ---------
            val txt5 = TextView(this)
            txt5.id = cont + 200
            txt5.layoutParams = lp2
            txt5.text =p.num_people.toString()
            txt5.gravity = Gravity.END

            //---------- Reserva Date -------------------------
            val txt2 = TextView(this)
            txt2.id = cont + 400
            txt2.layoutParams = lp3

            //---------- Reserva Start ------------------------
            val txt3 = TextView(this)
            txt3.text = p.start.toDate().toString()
            val parts = txt3.text.split(" ")

            val config: Date = SimpleDateFormat("MMM d kk:mm:ss YYYY").parse("${parts[1]} ${parts[2]} ${parts[3]} ${parts[5]}")

            txt3.id = cont + 600
            txt3.layoutParams = lp3
            if(config.hours <= 9) {
                if(config.minutes <= 9) {
                    txt3.text = "Start time: 0${config.hours}:0${config.minutes}"
                }
                else {
                    txt3.text = "Start time: 0${config.hours}:${config.minutes}"
                }
            }
            else {
                if (config.minutes <= 9) {
                    txt3.text = "Start time: ${config.hours}:0${config.minutes}"
                }
                else {
                    txt3.text = "Start time: ${config.hours}:${config.minutes}"
                }
            }

            //------------ Reserva End ------------------------
            val txt4 = TextView(this)
            txt4.text = p.end.toDate().toString()
            val parts2 = txt4.text.split(" ")

            val config2: Date = SimpleDateFormat("MMM dd kk:mm:ss YYYY").parse("${parts2[1]} ${parts2[2]} ${parts2[3]} ${parts2[5]}")
            txt2.text = "Date: ${parts2[2]}/${config2.month}/${config2.year}"

            txt4.id = cont + 800
            txt4.layoutParams = lp3

            if(config2.hours <= 9) {
                if(config2.minutes <= 9) {
                    txt4.text = "End time: 0${config2.hours}:0${config2.minutes}"
                }
                else {
                    txt4.text = "End time: 0${config2.hours}:${config2.minutes}"
                }
            }
            else {
                if (config2.minutes <= 9) {
                    txt4.text = "End time: ${config2.hours}:0${config2.minutes}"
                }
                else {
                    txt4.text = "End time: ${config2.hours}:${config2.minutes}"
                }
            }

            //----------- Preferences -------------------------
            val pref = p.preferences
            var barra = 0
            var arcade = 0
            var tv = 0

            val tam = pref.size
            var i = 0
            while( i < tam) {
                if(pref[i].trim() == "near bar")
                    barra = 1
                if (pref[i].trim() == "near arcade")
                    arcade = 1
                if (pref[i].trim() == "near tv")
                    tv = 1
                i+=1
            }

            val img1 = ImageView(this)
            img1.id = cont + 1000
            img1.layoutParams = lp3
            img1.setImageResource(R.drawable.ic_wine)

            if(barra == 0)
                img1.setColorFilter(Color.parseColor("#ACACAC"))

            val img2 = ImageView(this)
            img2.id = cont + 1200
            img2.layoutParams = lp3
            img2.setImageResource(R.drawable.ic_arcade)

            if(arcade == 0)
                img2.setColorFilter(Color.parseColor("#ACACAC"))

            val img3 = ImageView(this)
            img3.id = cont + 1400
            img3.layoutParams = lp3
            img3.setImageResource(R.drawable.ic_tv)

            if(tv == 0)
                img3.setColorFilter(Color.parseColor("#ACACAC"))

            val img4 = ImageView(this)
            img4.id = cont + 1600
            img4.layoutParams = lp3
            img4.setImageResource(R.drawable.ic_people)

            //------------- Adding relative layout ----------------------

            val paramsState: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsState.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsState.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsState.setMargins(10,10,0,0)
1
            val paramsDate: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsDate.addRule(RelativeLayout.BELOW, txt1.id)
            paramsDate.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsDate.setMargins(10,10,0,0)

            val paramsStart: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsStart.addRule(RelativeLayout.BELOW, txt2.id)
            paramsStart.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsStart.setMargins(10,10,0,0)

            val paramsEnd: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsEnd.addRule(RelativeLayout.BELOW, txt3.id)
            paramsEnd.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsEnd.setMargins(10,10,0,10)

            val paramsPeople: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_END)
            paramsPeople.setMargins(0,10,20,0)

            val paramsPref1: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPref1.addRule(RelativeLayout.BELOW, txt4.id)
            paramsPref1.addRule(RelativeLayout.ALIGN_PARENT_END)
            paramsPref1.setMargins(10,10,20,10)

            val paramsPref2: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPref2.addRule(RelativeLayout.BELOW, txt4.id)
            paramsPref2.addRule(RelativeLayout.START_OF, img1.id)
            paramsPref2.setMargins(10,10,20,10)

            val paramsPref3: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPref3.addRule(RelativeLayout.BELOW, txt4.id)
            paramsPref3.addRule(RelativeLayout.START_OF, img2.id)
            paramsPref3.setMargins(10,10,20,10)

            val paramsPref4: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPref4.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsPref4.addRule(RelativeLayout.START_OF, txt5.id)
            paramsPref4.setMargins(0,10,20,0)

            // Create RelativeLayout
            val relative = RelativeLayout(this)
            val lp: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            lp.setMargins(10,10,10,0)

            relative.addView(txt1, paramsState)
            relative.addView(txt2, paramsDate)
            relative.addView(txt3, paramsStart)
            relative.addView(txt4, paramsEnd)
            relative.addView(txt5, paramsPeople)
            relative.addView(img4, paramsPref4)
            relative.addView(img1, paramsPref1)
            relative.addView(img2, paramsPref2)
            relative.addView(img3, paramsPref3)

            card.addView(relative)

            llFood.addView(card)
        }
    }
}
