package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_food.*

class FoodActivity : AppCompatActivity() {

    private var foods = arrayListOf<DevFoodPlates>()
    val db = FirebaseFirestore.getInstance()
    var total = 0
    private lateinit var fbtn1 : ButtonComponent
    private lateinit var fbtn2 : ButtonComponent
    private lateinit var fbtn3 : ButtonComponent
    private lateinit var fbtn4 : ButtonComponent
    private lateinit var fbtn5 : ButtonComponent
    private lateinit var fbtn6 : ButtonComponent
    private lateinit var fbtn7 : ButtonComponent
    private lateinit var fbtn8 : ButtonComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.food_activity)
        //------------------------------------------------------------------------

        val cache = CacheReference.getLru()

        transformdata()
        cargar()

        goToDrinks.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn3.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            startActivity(intent)
            finish()
        }

        btnAddOrder.setOnClickListener {
            var order = ""
            if(fbtn1.get().toInt()>0){
                val str = foods[0].name.plus(",").plus(foods[0].price).plus(",").plus(fbtn1.get().toInt())

                order=order.plus(str).plus("|")
            }
            if(fbtn2.get().toInt()>0){
                val str = foods[1].name.plus(",").plus(foods[1].price).plus(",").plus(fbtn2.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn3.get().toInt()>0){
                val str = foods[2].name.plus(",").plus(foods[2].price).plus(",").plus(fbtn3.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn4.get().toInt()>0){
                val str = foods[3].name.plus(",").plus(foods[3].price).plus(",").plus(fbtn4.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn5.get().toInt()>0){
                val str = foods[4].name.plus(",").plus(foods[4].price).plus(",").plus(fbtn5.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn6.get().toInt()>0){
                val str = foods[5].name.plus(",").plus(foods[5].price).plus(",").plus(fbtn6.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn7.get().toInt()>0){
                val str = foods[6].name.plus(",").plus(foods[6].price).plus(",").plus(fbtn7.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn8.get().toInt()>0){
                val str = foods[7].name.plus(",").plus(foods[7].price).plus(",").plus(fbtn8.get().toInt())
                order=order.plus(str).plus("|")
            }

            if(cache.get("Order")==null){
                Log.d("CACHE","NO EXISTE")
                cache.put("Order",order)
            }
            else{
                Log.d("CACHE","EXISTE")
                val o = cache.get("Order")
                cache.remove("Order")
                val no = o.plus(order)
                cache.put("Order",no)
            }

            val snackbar = Snackbar.make(drawer_layout,
                "The items have been added to the order",
                Snackbar.LENGTH_INDEFINITE)

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()

                val intent = Intent(this, FoodActivity::class.java)
                startActivity(intent)
                finish()
            }
            snackbar.show()
        }
    }

    private fun transformdata(){
        val path = "FoodPlates"
        db.collection(path)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    val cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data.toString())
                        var  str = ""
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" |")
                        }

                        val part = str.split(" |")
                        val food = DevFoodPlates(part[1],part[2],part[3],part[0].toInt())
                        foods.add(food)

                        if(foods.size == cont)
                        {
                            cargar()
                        }
                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                }
            }
    }

    @SuppressLint("SetTextI18n")
    fun cargar() {
        val cache =CacheReference.getLru()
        txtTotalFood.text = "\$ $total"

        val llFood = findViewById<LinearLayout>(R.id.llFood)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)

        var cont = 0
        for(drink in foods) {
            val card = CardView(this)
            card.id = cont
            cont+=1
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val txt1 = TextView(this)
            txt1.layoutParams = lp2
            txt1.text = drink.name
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.layoutParams = lp2
            txt2.text = drink.price.toString()
            txt2.gravity = Gravity.END
            txt2.setTextColor(Color.BLUE)

            val llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.HORIZONTAL

            llayoutV.addView(txt1)
            llayoutV.addView(txt2)

            val llayoutV2 = LinearLayout(this)
            llayoutV2.layoutParams = lp
            llayoutV2.orientation = LinearLayout.HORIZONTAL

            val txt3 = TextView(this)
            txt3.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1.5f)
            txt3.text = drink.description

            val llayoutV3 = LinearLayout(this)
            llayoutV3.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)
            llayoutV3.orientation = LinearLayout.HORIZONTAL

            val btnComponent = ButtonComponent(this)
            val btn = btnComponent.buttonComponent()
            btn.gravity = Gravity.END

            if(cont==1){fbtn1 = btnComponent}
            if(cont==2){fbtn2 = btnComponent}
            if(cont==3){fbtn3 = btnComponent}
            if(cont==4){fbtn4 = btnComponent}
            if(cont==5){fbtn5 = btnComponent}
            if(cont==6){fbtn6 = btnComponent}
            if(cont==7){fbtn7 = btnComponent}
            if(cont==8){fbtn8 = btnComponent}

            llayoutV2.addView(txt3)
            llayoutV3.addView(btn)
            llayoutV2.addView(llayoutV3)

            val llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.VERTICAL
            llayout.addView(llayoutV)
            llayout.addView(llayoutV2)

            card.addView(llayout)

            llFood.addView(card)

            btnComponent.buttonPlus.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo =="1"){
                    if(limit!=0){
                        var new = (btnComponent.get().toInt()+1)*drink.price
                        if(tot+new >= limit){
                            val snackbar1 = Snackbar.make(llFood, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                            snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                            snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                            snackbar1.setAction("Ok" ) {
                                snackbar1.dismiss()
                            }
                            snackbar1.show()
                        }
                        else{
                            cache.remove("total")
                            cache.put("total", (tot+new).toString())
                            btnComponent.plus()
                            if(btnComponent.get().toInt() < 100) {
                                total += drink.price
                                txtTotalFood.text = "\$ $total"
                            }
                        }
                    }
                    else{
                        btnComponent.plus()
                        if(btnComponent.get().toInt() < 100) {
                            total += drink.price
                            txtTotalFood.text = "\$ $total"
                        }
                    }
                }
                else{
                    btnComponent.plus()
                    if(btnComponent.get().toInt() < 100) {
                        total += drink.price
                        txtTotalFood.text = "\$ $total"
                    }
                }

            }
            btnComponent.buttonLess.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo=="1"){
                    if(btnComponent.get().toInt()>0){
                        var new = tot - drink.price
                        cache.remove("total")
                        cache.put("total", (new).toString())
                        if(btnComponent.get().toInt() != 0 && total > 0) {
                            btnComponent.less()
                            total -= drink.price
                            txtTotalFood.text = "\$ $total"
                        }
                    }
                }
                else{
                    if(btnComponent.get().toInt() != 0 && total > 0) {
                        btnComponent.less()
                        total -= drink.price
                        txtTotalFood.text = "\$ $total"
                    }
                }

            }
        }
    }
}
