package com.example.bbcontrol

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.type.Date
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.etDate
import java.util.*

class ProfileActivity : AppCompatActivity() {

    var edit = 0
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_profile)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }
        cargar()
        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemOrder -> {
                    finish()
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_profile)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_profile, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    finish()
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_profile)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    finish()
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_profile)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_profile, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = "Cart"
        //------------------------------------------------------------------------

        btnRegister.setOnClickListener {

            if(edit == 0){
                btnRegister.text = getString(R.string.login_register)
                etName.isEnabled = true
                etLast.isEnabled = true
                etDate.isEnabled = true
                etMail.isEnabled = true
                etPhone.isEnabled = true

                edit = 1
            }
            else if(edit == 1){
                btnRegister.text = getString(R.string.edit)
                etName.isEnabled = false
                etLast.isEnabled = false
                etDate.isEnabled = false
                etMail.isEnabled = false
                etPhone.isEnabled = false
                val sharedPref = getSharedPreferences("USERINFO", 0)
                Log.d("SP",sharedPref.all.toString())
                val loguser = sharedPref.getString("UserId","").toString().replace(" ","")
                Log.d("USER",loguser)
                db.collection("Customers")
                    .whereEqualTo("id",loguser)
                    .get()
                    .addOnSuccessListener { documents ->
                        for (document in documents) {
                            var str = ""
                            val datos = Pair(document.id, document.data)
                            for (dat in datos.second?.values!!) {
                                str = str.plus(dat.toString()).plus(" |")
                            }
                            val part = str.split(" |")
                            var date= Timestamp(part[5].split(", ")[0].split("=")[1].toLong(),part[5].split(", ")[1].split("=")[1].split(")")[0].toInt())
                            println(part)
                            println(date.toDate().toString())
                            println(etDate.text.toString())
                            var new = etDate.text.toString().split("/")
                            var nd = Date(new[2].toInt()-1900,new[1].toInt(),new[0].toInt())
                            println(nd.toString())
                            var n = Timestamp(nd)
                            println(n)
                            var userRef = db.collection("Customers").document(loguser)
                            if(etName.text.toString() != part[1].toString()){
                                Log.d("CHANGE",etName.text.toString())
                                    userRef.update("firstName",etName.text.toString())
                                    .addOnSuccessListener {
                                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                    }
                            }
                            if(etLast.text.toString() != part[0].toString()){
                                Log.d("CHANGE",etLast.text.toString())
                                userRef.update("lastName",etLast.text.toString())
                                    .addOnSuccessListener {
                                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                    }
                            }
                            if(etMail.text.toString() != part[6].toString()){
                                Log.d("CHANGE",etMail.text.toString())
                                userRef.update("email",etMail.text.toString())
                                    .addOnSuccessListener {
                                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                    }
                            }
                            if(etPhone.text.toString() != part[2].toString().replace(".","")){
                                Log.d("CHANGE",etPhone.text.toString())
                                userRef.update("phoneNumber",etPhone.text.toString().toLong())
                                    .addOnSuccessListener {
                                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                    }
                            }
                            if(etDate.text.toString() != date.toDate().toString()){
                                Log.d("CHANGE",etDate.text.toString())
                                userRef.update("birthDate",n)
                                    .addOnSuccessListener {
                                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                    }
                            }
                        }
                    }
                edit = 0
            }


        }
    }
    fun cargar(){

        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("SP",sharedPref.all.toString())
        val loguser = sharedPref.getString("UserId","").toString().replace(" ","")
        Log.d("USER",loguser)
        db.collection("Customers")
            .whereEqualTo("id",loguser)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    var str = ""
                    val datos = Pair(document.id, document.data)
                    for (dat in datos.second?.values!!) {
                        str = str.plus(dat.toString()).plus(" |")
                    }
                    val part = str.split(" |")
                    var date= Timestamp(part[5].split(", ")[0].split("=")[1].toLong(),part[5].split(", ")[1].split("=")[1].split(")")[0].toInt())
                    println(part)
                    println(date.toDate().toString())
                    etName.setText( part[1].toString())
                    etLast.setText( part[0].toString())
                    etDate.setText(date.toDate().toString() )
                    etMail.setText( part[6].toString())
                    etPhone.setText( part[2].toString().replace(".",""))
                }
            }
    }

    fun setDate(view: View) {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dateSpinner = DatePickerDialog(this, DatePickerDialog.OnDateSetListener
        { datePicker, year, monthOfYear, dayOfMonth ->
            etDate.setText("$dayOfMonth/$monthOfYear/$year")
        }, year-18, month, day)
        dateSpinner.datePicker.maxDate = System.currentTimeMillis() - 18L*365*24*60*60*1001
        dateSpinner.show()
    }
}