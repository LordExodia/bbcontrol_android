package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_order.*

class OrderActivity : AppCompatActivity() {

    private var orders = arrayListOf<DevOrders>()
    val db = FirebaseFirestore.getInstance()
    private lateinit var Currentuser:String
    private lateinit var orderItems: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_order)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    finish()
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_order)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_order, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    finish()
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_order)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    finish()
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_order)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_order, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.my_order_activity)
        //------------------------------------------------------------------------

        if( intent.hasExtra("Order")) {
            orderItems = intent.getStringExtra("Order")
        }
        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("SP",sharedPref.all.toString())
        Currentuser = sharedPref.getString("UserId","").toString()
        transformdata()
    }

    private fun transformdata(){
        val path = "Orders"

        db.collection(path)
            .whereEqualTo("idUser", Currentuser.split(",")[0])
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    for (document in task.result?.documents!!) {
                        var  str = ""
                        lateinit var t : Timestamp
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            if (dat == null){
                                str = str.plus("").plus(" |")
                            }
                            else{
                                if((dat.javaClass.name)=="com.google.firebase.Timestamp") {
                                    t = dat as Timestamp
                                }
                                else{
                                    str = str.plus(dat.toString()).plus(" |")
                                }
                            }
                        }
                        val part = str.split(" |")
                        if(part[4].toIntOrNull() != null) {
                            val order = DevOrders(
                                part[0],
                                part[1].toInt(),
                                t,
                                part[5],
                                part[4].toInt(),
                                part[2],
                                part[3].toInt(),
                                arrayListOf()
                            )
                            orders.add(order)
                        }
                        else {
                            val order = DevOrders(
                                part[0],
                                part[1].toInt(),
                                t,
                                part[4],
                                part[5].toInt(),
                                part[2],
                                part[3].toInt(),
                                arrayListOf()
                            )
                            orders.add(order)
                        }
                    }
                    Log.d("DB","Se OBTIENE TODA LAS ORDENES")
                    getproducts()
                }
            }
    }

    private fun getproducts() {
        var cont = 0
        for ( order in orders ) {
            val id = order.id
            val path = "Orders/".plus(id).plus("/products")
            db.collection(path)
                .get()
                .addOnCompleteListener { task: Task<QuerySnapshot> ->
                    if (task.isSuccessful) {
                        for (document in task.result?.documents!!) {
                            var  str = ""
                            val datos = Pair(document.id,document.data)
                            for (dat in datos.second?.values!!){
                                //println(dat)
                                str = str.plus(dat.toString()).plus(" |")
                            }
                            order.products.add(str)
                            println()
                        }
                        Log.d("DB","Se OBTIENE TODOS LOS PRODUCTOS")
                        if(cont == (orders.size-1)){
                            cargar()
                        }
                        cont+=1
                    }
                }
        }
    }

    private fun cargar() {
        Log.d("CARGA", "EMPIEZA LA CARGA")
        val lOrders = findViewById<LinearLayout>(R.id.lorders)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0
        for( order in orders) {
            var str=""

            for (a in order.products){
                val parts = a.split("|")
                str = str.plus(parts[4]).plus(" ").plus(parts[3]).plus("    ").plus(parts[0]).plus("   ").plus(parts[1]).plus("\n")
            }

            val card = CardView(this)
            card.id = cont
            cont+=1
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val txt1 = TextView(this)
            txt1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
            txt1.text = getString(R.string.order).plus(cont.toString())
            txt1.gravity = Gravity.START
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.layoutParams = lp2
            if (order.state == 0 ){
                txt2.text = getString(R.string.in_progress)
            }
            else{
                txt2.text = getString(R.string.done)
            }


            val txt3 = TextView(this)
            txt3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
            txt3.gravity = Gravity.END
            txt3.setTextColor(Color.BLUE)
            txt3.text = "$".plus(order.total.toString())

            val txt4 =TextView(this)
            txt4.layoutParams = lp2
            txt4.text = str

            val llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.VERTICAL

            val llayoutV2 = LinearLayout(this)
            llayoutV2.layoutParams = lp
            llayoutV2.orientation = LinearLayout.HORIZONTAL

            llayoutV2.addView(txt1)
            llayoutV2.addView(txt3)

            llayoutV.addView(llayoutV2)
            llayoutV.addView(txt4)
            llayoutV.addView(txt2)

            val llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.HORIZONTAL

            llayout.addView(llayoutV)

            card.addView(llayout)

            lOrders.addView(card)
            card.setOnClickListener()
            {
                Toast.makeText(this, "PRUEBA".plus(cont),Toast.LENGTH_SHORT)
            }
        }
    }
}
