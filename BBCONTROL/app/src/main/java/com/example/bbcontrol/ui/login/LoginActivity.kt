package com.example.bbcontrol.ui.login

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.bbcontrol.*
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    private lateinit var auth: FirebaseAuth

    private lateinit var user : FirebaseUser

    val db = FirebaseFirestore.getInstance()

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        Thread.sleep(500)
        setTheme(R.style.AppTheme)

        val cache = CacheReference.getLru()
        cache.put("drunk","0")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()


        textView9.setOnClickListener {
            val intent = Intent(this, Registerctivity::class.java)
            intent.putExtra("Current", username.text.toString())
            startActivity(intent)
        }

        txtResetPass.setOnClickListener {
            val intent = Intent(this, RecoverPassActivity::class.java)
            startActivity(intent)
        }

        login.setOnClickListener {

            val online = ConnectionSingleton.isOnline(this.applicationContext)
            if (online){
                when {
                    username.text.toString().contains("@adminbbc.com") -> {
                        Log.d("SIGNIN","ADMIN_BBC")
                        loading.visibility = View.VISIBLE
                        Log.d(TAG, "signIn:$username")
                        auth.signInWithEmailAndPassword(username.text.toString(), password.text.toString()).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                user = auth.currentUser!!
                                println(user.toString())
                                loginViewModel.login(user)

                                val intent = Intent(this, PromotionActivity::class.java)
                                intent.putExtra("Current", username.text.toString())
                                startActivity(intent)

                            } else {
                                val snackbar = Snackbar.make(container, "Incorrect Password or email",
                                    Snackbar.LENGTH_INDEFINITE)

                                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                                snackbar.setAction("Ok") {
                                    snackbar.dismiss()
                                }
                                snackbar.show()
                            }
                        }
                    }
                    username.text.toString().contains("@bbc.com") -> {
                        Log.d("SIGNIN","WAITER_BBC")
                        loading.visibility = View.VISIBLE
                        Log.d(TAG, "signIn:$username")
                        auth.signInWithEmailAndPassword(username.text.toString(), password.text.toString()).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                user = auth.currentUser!!
                                println(user.toString())
                                loginViewModel.login(user)

                                val intent = Intent(this, WaiterActivity::class.java)
                                intent.putExtra("Current", username.text.toString())
                                startActivity(intent)

                            } else {
                                val snackbar = Snackbar.make(container, "Incorrect Password or email",
                                    Snackbar.LENGTH_INDEFINITE)

                                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                                snackbar.setAction("Ok") {
                                    snackbar.dismiss()
                                }
                                snackbar.show()
                            }
                        }
                    }
                    else -> {
                        loading.visibility = View.VISIBLE
                        Log.d(TAG, "signIn:$username")
                        auth.signInWithEmailAndPassword(username.text.toString(), password.text.toString()).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                user = auth.currentUser!!
                                println(user.toString())
                                loginViewModel.login(user)

                                val intent = Intent(this, LoadingUser::class.java)
                                intent.putExtra("Current", username.text.toString())
                                startActivity(intent)

                            } else {
                                val snackbar = Snackbar.make(container, "Incorrect Password or email",
                                    Snackbar.LENGTH_INDEFINITE)

                                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                                snackbar.setAction("Ok") {
                                    snackbar.dismiss()
                                }
                                snackbar.show()
                            }
                        }
                    }
                }

            }
            else {
                val snackbar = Snackbar.make(container, "No internet connection to any Network",
                    Snackbar.LENGTH_INDEFINITE)

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }


        }

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid

            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            else if (loginResult.success != null) {

                updateUiWithUser(loginResult.success)
            }
            setResult(Activity.RESULT_OK)
            //Complete and destroy login activity once successful
            finish()
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                println("CLICK")
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->

                        loginViewModel.login(user)

                }
                false
            }
        }
    }

    private fun updateUiWithUser(model: LoggedInUserView) {

        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    companion object {
        private const val TAG = "EmailPassword"
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        val snackbar = Snackbar.make(container, errorString,
            Snackbar.LENGTH_INDEFINITE)

        snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
        snackbar.show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}


