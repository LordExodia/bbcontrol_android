package com.example.bbcontrol

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_table.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class TableActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    private var arcade = 0
    private var tv = 0
    private var bar = 0

    /***** OnCreate *****/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_table)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_table)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_table)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_table)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_table, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_table)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_table)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_table, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.table_activity)
        //------------------------------------------------------------------------

        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("SP",sharedPref.all.toString())
        val loguser = sharedPref.getString("UserId","").toString()

        /***** Validar Seleccion *****/
        val gray = R.color.gray
        ivTv.setBackgroundColor(resources.getColor(gray))
        ivArcade.setBackgroundColor(resources.getColor(gray))
        ivWine.setBackgroundColor(resources.getColor(gray))

        ivArcade.setOnClickListener {
            if(arcade == 0) {
                arcade = 1
                val blue = R.color.blue
                ivArcade.setBackgroundColor(resources.getColor(blue))
            } else {
                arcade = 0
                ivArcade.setBackgroundColor(resources.getColor(gray))
            }
        }

        ivTv.setOnClickListener {
            if(tv == 0) {
                tv = 1
                val yellow = R.color.yellow
                ivTv.setBackgroundColor(resources.getColor(yellow))
            } else {
                tv = 0
                ivTv.setBackgroundColor(resources.getColor(gray))
            }
        }

        ivWine.setOnClickListener {
            if(bar == 0) {
                bar = 1
                val purple = R.color.purple
                ivWine.setBackgroundColor(resources.getColor(purple))
            } else {
                bar = 0
                ivWine.setBackgroundColor(resources.getColor(gray))
            }
        }
        /***** Validar Seleccion *****/

        /***** Reservar *****/
        btnRegister.setOnClickListener {
            reservar(loguser)

        }
    }
    /***** OnCreate *****/

    /***** Set time date *****/
    fun setDate(view: View) {
        val c = Calendar.getInstance()
        val day = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val year = c.get(Calendar.YEAR)

        val dateSpinner = DatePickerDialog(this, DatePickerDialog.OnDateSetListener
        { datePicker, year, monthOfYear, dayOfMonth ->
            var month =monthOfYear+1
            etDate.setText("$dayOfMonth/$month/$year")
        }, year, month, day + 1)
        dateSpinner.datePicker.minDate = System.currentTimeMillis() + 1L*24*60*60*1000

        dateSpinner.show()
    }
    /***** Set time date *****/

    /***** Set time start *****/
    fun setStartTime(view: View) {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)
        val timePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener
        { view, h, m ->
            if(m <= 9) {
                etStartTime.setText("$h:0$m")
            }
            else{
                etStartTime.setText("$h:$m")
            }
        }, hour, minute,false)
        timePicker.show()
    }
    /***** Set time start *****/

    /***** Set time end *****/
    @SuppressLint("SetTextI18n")
    fun setEndTime(view: View) {
        val c = Calendar.getInstance()
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minute = c.get(Calendar.MINUTE)

        val timePicker = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener
        { view, h, m ->
            if(m <= 9) {
                etEndTime.setText("$h:0$m")
            }
            else{
                etEndTime.setText("$h:$m")
            }
        }, hour, minute,false)

        timePicker.show()
    }
    /***** Set time end *****/

    /***** Crear Nueva Reserva *****/
    fun reservar(loguser: String) {
        val online = ConnectionSingleton.isOnline(this.applicationContext)
        if(online){
            val replyIntent = Intent()
            if (TextUtils.isEmpty(etDate.text)  || TextUtils.isEmpty(etStartTime.text) ||
                TextUtils.isEmpty(etEndTime.text) || TextUtils.isEmpty(etPeople.text)) {
                val snackbar = Snackbar.make(drawer_layout_table,
                    "Complete all the fields",
                    Snackbar.LENGTH_INDEFINITE)

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            } else {
                val date = etDate.text.toString()
                val start = etStartTime.text.toString()
                if(start.contains(":0")){
                    start.replace(":0",":")
                }
                val end = etEndTime.text.toString()
                if(end.contains(":0")){
                    end.replace(":0",":")
                }
                val people = etPeople.text.toString().toInt()

                val compStart = start.split(":")
                val compEnd = end.split(":")
                val hourStart = compStart[0].toInt()
                val hourEnd = compEnd[0].toInt()

                if (hourStart >= hourEnd - 2) {
                    val id = UUID.randomUUID().toString()
                    val datestart = date.plus(" ").plus(start).plus(":00")
                    val dateend = date.plus(" ").plus(end).plus(":00")
                    val date1: Date = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(datestart)
                    val date2: Date = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dateend)
                    var i = 0
                    val pref: ArrayList<String> = ArrayList()
                    if (bar>0){
                        pref.add(i,"near bar")
                        i+=1
                    }
                    if (arcade>0){
                        pref.add(i,"near arcade")
                        i+=1
                    }
                    if (tv>0){
                        pref.add(i,"near tv")
                        i+=1
                    }

                    val creat = hashMapOf(
                        "date" to  Timestamp.now(),
                        "end" to Timestamp(date2),
                        "id" to id,
                        "num_people" to people,
                        "start" to Timestamp(date1),
                        "user_Id" to loguser,
                        "preferences" to pref
                    )
                    println(creat)
                    db.collection("Reservations").document(id).set(creat)

                }
                else {
                    val snackbar = Snackbar.make(drawer_layout_table,
                        "A reservation can't last more that 3 hours",
                        Snackbar.LENGTH_INDEFINITE)

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok") {
                        snackbar.dismiss()
                    }
                    snackbar.show()
                }
            }
            val intent = Intent(this, NewReservaActivity::class.java)
            startActivity(intent)
            finish()
        }
        else {
            val snackbar = Snackbar.make(drawer_layout_table,
                "No internet connection to any Network",
                Snackbar.LENGTH_INDEFINITE)

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()
            }
            snackbar.show()
        }
    }
    /***** Crear Nueva Reserva *****/
}
