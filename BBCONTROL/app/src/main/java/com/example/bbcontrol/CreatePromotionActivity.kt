package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_create_promotion.*
import kotlinx.android.synthetic.main.activity_create_promotion.btnRegister
import kotlinx.android.synthetic.main.activity_registerctivity.*

class CreatePromotionActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_promotion)

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.daily_promotions)
        //------------------------------------------------------------------------
        btnRegister.setOnClickListener {
            val online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                val creat = hashMapOf(
                    "active" to checkPromo.isChecked,
                    "description" to etDescription.text.toString(),
                    "name" to etPromo.text.toString(),
                    "price" to etPrice.text.toString().toInt()
                )
                db.collection("Promotions")
                    .add(creat)
                    .addOnSuccessListener { documentReference ->
                        val usuario = db.collection("Promotions").document(documentReference.id)
                        usuario
                            .update("id", documentReference.id)
                            .addOnSuccessListener {
                                Log.d(
                                    "PROMO",
                                    "DocumentSnapshot successfully updated!"
                                )
                                val intent = Intent(this, PromotionActivity::class.java)
                                startActivity(intent)
                            }
                            .addOnFailureListener { e ->
                                Log.w(
                                    "PROMO",
                                    "Error updating document",
                                    e
                                )
                            }
                        Log.d("PROMO", "DocumentSnapshot added with ID: ${documentReference.id}")
                    }
            }
            else{
                val snackbar = Snackbar.make(register,
                    "No internet connection to any Network",
                    Snackbar.LENGTH_INDEFINITE)

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
    }
}