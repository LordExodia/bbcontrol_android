package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_waiter.*
import kotlinx.android.synthetic.main.admin_view.*

class WaiterActivity : AppCompatActivity() {

    var orders = arrayListOf<DevOrders>()
    lateinit var waiterId: String
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waiter)
        Toast.makeText(this, "WAITERS", Toast.LENGTH_SHORT)
        var current :String = intent.getStringExtra("Current")

        cargarOrders(current)

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.order)
        //------------------------------------------------------------------------

        btnTable.setOnClickListener {
            val intent = Intent(this, WaiterTablesActivity::class.java)
            intent.putExtra("Current", current)
            startActivity(intent)
        }

    }

    fun cargarOrders(current: String) {
        val path = "BBCEmployees"

        db.collection(path)
            .whereEqualTo("email",current)
            .get()
            .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    var cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data)
                        var str = ""
                        var datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str =str.plus(dat.toString()).plus("|")
                        }
                        var parts = str.split("|")
                        println(parts)
                        waiterId = parts[5]

                    }
                    Log.d("DB","Se OBTIENE LA INFORMACION DEL WAITER")
                    db.collection("Orders")
                        .whereEqualTo("idWaiter",waiterId)
                        .get()
                        .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                            if (task.isSuccessful) {
                                var cont = task.result?.documents!!.size
                                for (document in task.result?.documents!!) {
                                    //println(document.data)
                                    var str = ""
                                    var datos = Pair(document.id,document.data)
                                    for (dat in datos.second?.values!!){
                                        str =str.plus(dat.toString()).plus("|")
                                    }
                                    var parts = str.split("|")
                                    println("--------------------------       $parts")
                                    var date = parts[3].split("(")[1].split(")")[0].split(", ")
                                    var time = Timestamp(date[0].split("=")[1].toLong(),date[1].split("=")[1].toInt())

                                    if(parts[5].toIntOrNull() == null) {
                                        var order = DevOrders(parts[0],parts[1].toInt(),time,parts[5],parts[6].toInt(),waiterId,parts[4].toInt(), ArrayList())
                                        orders.add(order)
                                    }
                                    else {
                                        var order = DevOrders(parts[0],parts[1].toInt(),time,parts[6],parts[5].toInt(),waiterId,parts[4].toInt(), ArrayList())
                                        orders.add(order)
                                    }

                                }
                                Log.d("DB","SE OBTIENEN LAS ORDENES")
                                cargar()
                            }
                        }
                }
            }
    }

    fun cargar(){
        var llOrders = findViewById<LinearLayout>(R.id.llOrders)

        var lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        var lp2 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)

        var i = 0
        for(order in orders) {
            i += 1
            var card = CardView(this)
            card.id = i + 800
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            var txt1 = TextView(this)
            txt1.id = i + 600
            txt1.layoutParams = lp2
            txt1.text = "Order ".plus(i)
            txt1.textSize = 20f

            var txt2 = TextView(this)
            txt2.id = i + 400
            txt2.layoutParams = lp2
            txt2.gravity = Gravity.END
            if (order.state==1){
                txt2.text = "Active"
                txt2.setTextColor(Color.GREEN)
            }
            else{
                txt2.text = "Inactive"
                txt2.setTextColor(Color.RED)
            }

            var txt3 = TextView(this)
            txt3.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1.5f)
            txt3.text = "Limit: $".plus(order.limit)

            var txt4 = TextView(this)
            txt4.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1.5f)
            txt4.text = "Total: $".plus(order.total)
            txt4.gravity = Gravity.END

            var llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.HORIZONTAL

            llayoutV.addView(txt1)
            llayoutV.addView(txt2)

            var llayoutV2 = LinearLayout(this)
            llayoutV2.layoutParams = lp
            llayoutV2.orientation = LinearLayout.HORIZONTAL

            llayoutV2.addView(txt3)
            llayoutV2.addView(txt4)

            var llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.VERTICAL
            llayout.addView(llayoutV)
            llayout.addView(llayoutV2)

            card.addView(llayout)

            llOrders.addView(card)
        }
    }
}
