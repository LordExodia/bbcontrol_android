package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {
    private var drinks = arrayListOf<DevAlcoholicDrink>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_menu, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_menu, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.alcoholic)
        //------------------------------------------------------------------------

        btn2.setOnClickListener {
            val intent = Intent(this, Menu2Activity::class.java)
            startActivity(intent)
            finish()
        }
        goToFood.setOnClickListener {
            val intent = Intent(this, FoodActivity::class.java)
            startActivity(intent)
            finish()
        }
        btn3.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            startActivity(intent)
            finish()
        }

        transformdata()
        cargar()
    }

    private fun transformdata() {
        val path = "Drinks/A1AX1eCQDVMq9uRSMnGe/Alcoholic Drinks/H6vBaPIidRlPTSFJDyAk/Beers"

        db.collection(path)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if(task.isSuccessful){
                    val cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!){
                        var  str = ""
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" ,")
                        }

                        val path2 = path.plus("/").plus(datos.first).plus("/Prices")
                        db.collection(path2)
                            .get()
                            .addOnCompleteListener { task2: Task<QuerySnapshot> ->
                                if(task.isSuccessful){
                                    for (document2 in task2.result?.documents!!){
                                        val datos2 = Pair(document2.id,document2.data)

                                        for (dat2 in datos2.second?.values!!){
                                            str = str.plus(dat2.toString()).plus(" ,")
                                        }
                                        val part = str.split(" ,")
                                        val drink = DevAlcoholicDrink(part[2],part[3],part[0],part[1],part[4].toInt(),part[5].toInt(),part[6].toInt(),part[7].toInt())
                                        drinks.add(drink)

                                        if(drinks.size == cont)
                                        {
                                            cargar()
                                        }
                                    }
                                }
                            }
                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                }
            }
    }

    private fun cargar() {
        val llDrink = findViewById<LinearLayout>(R.id.llDrinks)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0
        for(drink in drinks) {
            val card = CardView(this)
            card.id = cont
            cont+=1
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val img1 = ImageView(this)
            img1.layoutParams = LinearLayout.LayoutParams(260,260)
            Picasso.get().load(drink.image).into(img1)
            img1.setPadding(30,30,30,30)

            val txt1 = TextView(this)
            txt1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
            txt1.text = drink.name
            txt1.gravity = Gravity.START
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.layoutParams = lp2
            txt2.text = drink.description

            val txt3 = TextView(this)
            txt3.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
            txt3.gravity = Gravity.END
            txt3.setTextColor(Color.BLUE)
            txt3.text = drink.volume

            val llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.VERTICAL

            val llayoutV2 = LinearLayout(this)
            llayoutV2.layoutParams = lp
            llayoutV2.orientation = LinearLayout.HORIZONTAL

            llayoutV2.addView(txt1)
            llayoutV2.addView(txt3)

            llayoutV.addView(llayoutV2)
            llayoutV.addView(txt2)

            val llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.HORIZONTAL

            llayout.addView(img1)
            llayout.addView(llayoutV)

            card.addView(llayout)

            llDrink.addView(card)

            card.setOnClickListener()
            {
                val intent = Intent(this, CartActivity::class.java)

                val drink = drinks[card.id]
                intent.putExtra("name", drink.name)
                intent.putExtra("description", drink.description)
                intent.putExtra("volume", drink.volume)
                intent.putExtra("image", drink.image)
                intent.putExtra("priceGlass", drink.priceGlass)
                intent.putExtra("priceJar", drink.priceJar)
                intent.putExtra("pricePint", drink.pricePint)
                intent.putExtra("priceTower", drink.priceTower)
                startActivity(intent)
                finish()
            }
        }
    }
}
