package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_create_promotion.*
import kotlinx.android.synthetic.main.activity_create_promotion.btnRegister
import kotlinx.android.synthetic.main.activity_create_waiter.*
import kotlinx.android.synthetic.main.activity_registerctivity.*

class CreateWaiterActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_waiter)

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.waiter)
        //------------------------------------------------------------------------
        btnRegister.setOnClickListener {
            auth = FirebaseAuth.getInstance()
            val online = ConnectionSingleton.isOnline(this.applicationContext)
            if(online){
                auth.createUserWithEmailAndPassword(etWaiterMail.text.toString(), etWaiterPAss.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("CREATE", "createUserWithEmail:success")
                            auth.currentUser

                            val creat = hashMapOf(
                                "active" to true,
                                "email" to etWaiterMail.text.toString(),
                                "firstName" to etWaiterName.text.toString(),
                                "identification" to etwaiterIden.text.toString().toInt(),
                                "lastName" to etWaiterLast.text.toString(),
                                "ordersAmount" to 0,
                                "phoneNumber" to etWaiterPhone.text.toString().toLong()
                            )
                            println(creat)
                            db.collection("BBCEmployees")
                                .add(creat)
                                .addOnSuccessListener { documentReference ->
                                    val usuario = db.collection("BBCEmployees").document(documentReference.id)
                                    usuario
                                        .update("id", documentReference.id)
                                        .addOnSuccessListener { Log.d("UPDATE", "DocumentSnapshot successfully updated!") }
                                        .addOnFailureListener { e -> Log.w("UPDATE", "Error updating document", e) }
                                    Log.d("UPDATE", "DocumentSnapshot added with ID: ${documentReference.id}")

                                    val intent = Intent(this, AdminActivity::class.java)
                                    startActivity(intent)
                                }
                                .addOnFailureListener { e ->
                                    Log.w("UPDATE", "Error adding document", e)
                                }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("CREATE", "createUserWithEmail:failure", task.exception)

                            val snackbar = Snackbar.make(register,
                                "You cant enter email already in use",
                                Snackbar.LENGTH_INDEFINITE)

                            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                            snackbar.setAction("Ok") {
                                snackbar.dismiss()
                            }
                            snackbar.show()
                        }
                    }
            }
            else{
                val snackbar = Snackbar.make(register,
                    "No internet connection to any Network",
                    Snackbar.LENGTH_INDEFINITE)

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
    }
}