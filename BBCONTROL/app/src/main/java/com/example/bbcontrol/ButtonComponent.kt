package com.example.bbcontrol

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout

class ButtonComponent(v: Context) {

    var editText = EditText(v)
    var buttonLess = Button(v)
    var buttonPlus = Button(v)
    var linearLayout = LinearLayout(v)

    var lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

    fun buttonComponent(): LinearLayout {
        linearLayout.layoutParams = lp
        linearLayout.orientation = LinearLayout.HORIZONTAL

        editText.setText("0")
        editText.gravity = Gravity.CENTER
        editText.inputType = 0
        editText.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.7f)

        buttonLess.setText("-")
        buttonLess.textSize = 20f
        buttonLess.background = null
        buttonLess.setTextColor(Color.BLACK)
        buttonLess.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.5f)

        buttonPlus.setText("+")
        buttonPlus.textSize = 20f
        buttonPlus.background = null
        buttonPlus.setTextColor(Color.RED)
        buttonPlus.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.5f)

        linearLayout.addView(buttonLess)
        linearLayout.addView(editText)
        linearLayout.addView(buttonPlus)

        buttonPlus.setOnClickListener { plus() }
        buttonLess.setOnClickListener { less() }

        return linearLayout
    }

    fun get(): String {
        return editText.text.toString()
    }

    fun plus() {
        var num = editText.text.toString().toInt()
        if(num < 100) {
            editText.setText((num + 1).toString())
        }
    }

    fun less() {
        var num = editText.text.toString().toInt()
        if(num > 0) {
            editText.setText((num - 1).toString())
        }
    }
}