package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_drunk_mode.*

class DrunkModeActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drunk_mode)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_drunk)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    finish()
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_drunk)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    finish()
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_drunk)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {
                    val snackbar = Snackbar.make(
                        drawer_layout_drunk,
                        "Calling the waiter.",
                        Snackbar.LENGTH_INDEFINITE )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok" ) {
                        snackbar.dismiss()
                    }
                    snackbar.show()
                }
                R.id.itemControl -> {
                    finish()
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_drunk)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    val snackbar = Snackbar.make(drawer_layout_drunk,
                        "Log Out.",
                        Snackbar.LENGTH_INDEFINITE
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok") {
                        snackbar.dismiss()
                    }
                    snackbar.show()
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.drunk_activity)
        //------------------------------------------------------------------------


        val cache = CacheReference.getLru()
        val drunkValue = cache.get("drunk")

        drunkActivated.isChecked = drunkValue == "1"

        drunkActivated.setOnCheckedChangeListener { _, isChecked ->
            if(!isChecked) {
                showDialog()
            }
            else {
                val cache = CacheReference.getLru()
                cache.put("drunk","1")
            }
        }
    }

    private fun showDialog() {

        var operStr = ""
        var resultOper: Int = 0
        var result1: Int = 0
        var result2: Int = 0
        val num1 = (30..70).random()
        val num2 = (30..70).random()
        val oper = (0..1).random()

        when (oper) {
            0 -> {
                resultOper = num1 + num2
                result1 = ((resultOper-5)..(resultOper+5)).random()
                result2 = ((resultOper-5)..(resultOper+5)).random()
                operStr = "+"
            }
            1 -> {
                resultOper = num1 - num2
                result1 = ((resultOper-15)..(resultOper+15)).random()
                result2 = ((resultOper-15)..(resultOper+15)).random()
                operStr = "-"
            }
        }
        val resultados: ArrayList<Int> = ArrayList()
        resultados.add(resultOper)
        resultados.add(result1)
        resultados.add(result2)

        val alea1 = resultados.removeAt(((0 until resultados.size).random()))
        val alea2 = resultados.removeAt(((0 until resultados.size).random()))
        val alea3 = resultados.removeAt(((0 until resultados.size).random()))

        val select = getString(R.string.correct_answer)
        val textOpertion = " $num1 $operStr $num2 = "

        var dialog = AlertDialog.Builder(this)
        dialog.setCancelable(false)
        dialog.setTitle(select)
        dialog.setMessage(textOpertion)
        dialog.setPositiveButton("$alea1") { dialogs, switch ->
            if(alea1 == resultOper) {
                drunkActivated.isChecked = false
                val cache = CacheReference.getLru()
                cache.put("drunk","0")
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Good answer! Drunk Mode has been disabled.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
            else {
                drunkActivated.isChecked = true
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.setNegativeButton("$alea2") { dialogs, switch ->
            if(alea2 == resultOper) {
                drunkActivated.isChecked = false
                val cache = CacheReference.getLru()
                cache.put("drunk","0")
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Good answer! Drunk Mode has been disabled.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
            else {
                drunkActivated.isChecked = true
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.setNeutralButton("$alea3") { dialogs, switch ->
            if(alea3 == resultOper) {
                drunkActivated.isChecked = false
                val cache = CacheReference.getLru()
                cache.put("drunk","0")
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Good answer! Drunk Mode has been disabled.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
            else {
                drunkActivated.isChecked = true
                val snackbar = Snackbar.make(drawer_layout_drunk,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.show()
    }
}