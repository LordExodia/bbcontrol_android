package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_menu2.*
import kotlinx.android.synthetic.main.activity_menu2.btnAddOrder
import kotlinx.android.synthetic.main.activity_menu2.txtTotal

class Menu2Activity : AppCompatActivity() {
    private var drinks = arrayListOf<DevNonAlcoholicDrink>()
    val db = FirebaseFirestore.getInstance()
    var total = 0
    private lateinit var llDrinkNon : LinearLayout
    private lateinit var dbtn1 : ButtonComponent
    private lateinit var dbtn2 : ButtonComponent
    private lateinit var dbtn3 : ButtonComponent
    private lateinit var dbtn4 : ButtonComponent
    private lateinit var dbtn5 : ButtonComponent
    private lateinit var dbtn6 : ButtonComponent
    private lateinit var dbtn7 : ButtonComponent


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu2)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu2)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu2)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu2)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_menu2, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu2)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_menu2)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_menu2, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.non_alcoholic)
        //------------------------------------------------------------------------

        val cache =CacheReference.getLru()

        transformdata()
        cargar()

        btn1.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
            startActivity(intent)
            finish()
        }
        btnAddOrder.setOnClickListener {
            var order =""
            val snackbar2 = Snackbar.make(drawer_layout_menu2,
                "The items have been added to the order",
                Snackbar.LENGTH_INDEFINITE)

            snackbar2.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar2.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar2.setAction("Ok") {
                snackbar2.dismiss()
            }
            snackbar2.show()

            if(dbtn1.get().toInt()>0){
                val str = drinks[0].name.plus(",").plus(drinks[0].price).plus(",").plus(dbtn1.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn2.get().toInt()>0){
                val str = drinks[1].name.plus(",").plus(drinks[1].price).plus(",").plus(dbtn2.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn3.get().toInt()>0){
                val str = drinks[2].name.plus(",").plus(drinks[2].price).plus(",").plus(dbtn3.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn4.get().toInt()>0){
                val str = drinks[3].name.plus(",").plus(drinks[3].price).plus(",").plus(dbtn4.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn5.get().toInt()>0){
                val str = drinks[4].name.plus(",").plus(drinks[4].price).plus(",").plus(dbtn5.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn6.get().toInt()>0){
                val str = drinks[5].name.plus(",").plus(drinks[5].price).plus(",").plus(dbtn6.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(dbtn7.get().toInt()>0){
                val str = drinks[6].name.plus(",").plus(drinks[6].price).plus(",").plus(dbtn7.get().toInt())
                order=order.plus(str).plus("|")
            }

            if(cache.get("Order")==null){
                Log.d("CACHE","NO EXISTE")
                cache.put("Order",order)
            }
            else{
                Log.d("CACHE","EXISTE")
                val o = cache.get("Order")
                cache.remove("Order")
                val no = o.plus(order)
                cache.put("Order",no)
            }
            println(cache.get("Order"))

            val snackbar = Snackbar.make(drawer_layout_menu2,
                "The items have been added to the order",
                Snackbar.LENGTH_INDEFINITE)

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()

                val intent = Intent(this, Menu2Activity::class.java)
                startActivity(intent)
                finish()
            }
            snackbar.show()
        }

        goToFood.setOnClickListener {
            val intent = Intent(this, FoodActivity::class.java)
            startActivity(intent)
            finish()
        }

        btn3.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
    private fun transformdata() {
        val path = "Drinks/A1AX1eCQDVMq9uRSMnGe/Non-alcoholic Drinks"

        db.collection(path)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    val cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        var  str = ""
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str = str.plus(dat.toString()).plus(" ,")

                        }
                        val part = str.split(" ,")
                        val drink = DevNonAlcoholicDrink(part[2],part[0],part[1].toInt())
                        drinks.add(drink)

                        if(drinks.size == cont)
                        {
                            println(drinks.size)
                            cargar()
                        }
                    }
                    Log.d("DB","Se OBTIENE TODA LA INFORMACION")
                }
            }
    }
    @SuppressLint("SetTextI18n")
    fun cargar(){
        val cache =CacheReference.getLru()
        txtTotal.text = "\$ $total"

        llDrinkNon = findViewById(R.id.llDrinksNon)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)

        var i = 0

        for(drink in drinks) {
            i += 1
            val card = CardView(this)
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val img1 = ImageView(this)
            img1.layoutParams = LinearLayout.LayoutParams(260,260)
            Picasso.get().load(drink.image).into(img1)
            img1.setPadding(30,30,30,30)

            val txt1 = TextView(this)
            txt1.layoutParams = lp2
            txt1.text = drink.name
            txt1.textSize = 20f
            txt1.gravity = Gravity.START

            val txt2 = TextView(this)
            txt2.layoutParams = lp2
            txt2.text = drink.price.toString()
            txt2.gravity = Gravity.END
            txt2.setTextColor(Color.BLUE)

            val btnComponent = ButtonComponent(this)
            val btn = btnComponent.buttonComponent()
            if(i==1){dbtn1 = btnComponent}
            if(i==2){dbtn2 = btnComponent}
            if(i==3){dbtn3 = btnComponent}
            if(i==4){dbtn4 = btnComponent}
            if(i==5){dbtn5 = btnComponent}
            if(i==6){dbtn6 = btnComponent}
            if(i==7){dbtn7 = btnComponent}

            val llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.HORIZONTAL

            val llayout2 = LinearLayout(this)
            llayout2.layoutParams = lp
            llayout2.orientation = LinearLayout.HORIZONTAL

            val llayout3 = LinearLayout(this)
            llayout3.layoutParams = lp
            llayout3.orientation = LinearLayout.VERTICAL

            llayout2.addView(txt1)
            llayout2.addView(txt2)

            llayout3.addView(llayout2)
            llayout3.addView(btn)

            llayout.addView(img1)
            llayout.addView(llayout3)

            card.addView(llayout)

            llDrinkNon.addView(card)

            btnComponent.buttonPlus.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo =="1"){
                    if(limit!=0){
                        var new = (btnComponent.get().toInt()+1)*drink.price
                        if(tot+new >= limit){
                            val snackbar1 = Snackbar.make(llDrinkNon, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                            snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                            snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                            snackbar1.setAction("Ok" ) {
                                snackbar1.dismiss()
                            }
                            snackbar1.show()
                        }
                        else{
                            cache.remove("total")
                            cache.put("total", (tot+new).toString())
                            btnComponent.plus()
                            if(btnComponent.get().toInt() < 100) {
                                total += drink.price
                                txtTotal.text = "\$ $total"
                            }
                        }
                    }
                    else{
                        btnComponent.plus()
                        if(btnComponent.get().toInt() < 100) {
                            total += drink.price
                            txtTotal.text = "\$ $total"
                        }
                    }
                }
                else{
                    btnComponent.plus()
                    if(btnComponent.get().toInt() < 100) {
                        total += drink.price
                        txtTotal.text = "\$ $total"
                    }
                }

            }
            btnComponent.buttonLess.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo=="1"){
                    if(btnComponent.get().toInt()>0){
                        var new = tot - drink.price
                        cache.remove("total")
                        cache.put("total", (new).toString())
                        if(btnComponent.get().toInt() != 0 && total > 0) {
                            btnComponent.less()
                            total -= drink.price
                            txtTotal.text = "\$ $total"
                        }
                    }
                }
                else{
                    if(btnComponent.get().toInt() != 0 && total > 0) {
                        btnComponent.less()
                        total -= drink.price
                        txtTotal.text = "\$ $total"
                    }
                }

            }
        }
    }
}
