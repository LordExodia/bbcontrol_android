package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_waitertable.*

class WaiterTablesActivity : AppCompatActivity() {

    var tables = arrayListOf<DevTables>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waitertable)
        Toast.makeText(this, "TABLES", Toast.LENGTH_SHORT)
        var current :String = intent.getStringExtra("Current")

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.table)
        //------------------------------------------------------------------------

        cargarTables()

        btnOrder.setOnClickListener {
            val intent = Intent(this, WaiterActivity::class.java)
            intent.putExtra("Current", current)
            startActivity(intent)
            finish()
        }
    }

    fun cargarTables(){
        val path = "Tables"

        db.collection(path)
            .get()
            .addOnCompleteListener() { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    var cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data)
                        var str = ""
                        var datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str =str.plus(dat.toString()).plus("|")
                        }
                        var parts = str.split("|")
                        // println(parts)
                        var table = DevTables(parts[0].toBoolean(),parts[2].toInt(), parts[1].toInt())
                        println(table)
                        tables.add(table)

                        if(cont == tables.size) {
                            cargar()
                        }
                    }
                    Log.d("DB","SE OBTIENE TODA LA INFORMACION DE TABLES")
                }
            }
    }

    @SuppressLint("SetTextI18n")
    fun cargar(){
        var llTable= findViewById<LinearLayout>(R.id.llTables)

        var lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0
        for(table in tables) {
            val txt1 = TextView(this)
            txt1.id = cont + 800
            txt1.layoutParams = lp2
            txt1.text = "Num of Table: ${table.number}"
            txt1.textSize = 20f

            val txt3 = TextView(this)
            txt3.id = cont + 200
            txt3.layoutParams = lp2
            if (table.available){
                txt3.text = "Available"
                txt3.setTextColor(Color.GREEN)
            }
            else{
                txt3.text = "Unavailable"
                txt3.setTextColor(Color.RED)
            }

            val txt2 = TextView(this)
            txt2.id = cont + 400
            txt2.layoutParams = lp2
            txt2.text = "Available seats: ${table.seats}"
            txt2.textSize = 20f

            val card = CardView(this)
            card.id = cont + 600
            cont+=1
            card.layoutParams = lp2
            card.elevation = 10f
            card.useCompatPadding = true

            // Create RelativeLayout
            val relative = RelativeLayout(this)
            val lp: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            lp.setMargins(10,10,10,0)

            val paramsState: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsState.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsState.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsState.setMargins(10,10,0,0)

            val paramsDate: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsDate.addRule(RelativeLayout.BELOW, txt1.id)
            paramsDate.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsDate.setMargins(10,10,0,0)

            val paramsPeople: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_END)
            paramsPeople.setMargins(0,10,20,0)

            relative.addView(txt1, paramsState)
            relative.addView(txt2, paramsDate)
            relative.addView(txt3, paramsPeople)

            card.addView(relative)

            llTable.addView(card)
        }
    }
}