package com.example.bbcontrol

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_recover_pass.*

class RecoverPassActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recover_pass)

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.recover_activity)
        //------------------------------------------------------------------------

        sendMail.setOnClickListener {

            val snackbar = Snackbar.make(recoverPass,
                "An email with your password has been sent to ${mailAddress.text.toString()}",
                Snackbar.LENGTH_INDEFINITE)

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()
            }
            snackbar.show()

        }
    }
}