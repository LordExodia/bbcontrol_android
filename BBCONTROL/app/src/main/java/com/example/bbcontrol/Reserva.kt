package com.example.bbcontrol

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reserva_table")

class Reserva(

    @PrimaryKey(autoGenerate = true) val id: Int,

    @ColumnInfo(name = "id_usuario") val idUsuario: String,

    @ColumnInfo(name = "id_mesa") val idMesa: Int,

    @ColumnInfo(name = "fecha") val fecha: String,

    @ColumnInfo(name = "hora_inicio") val horaInicio: String,

    @ColumnInfo(name = "hora_fin") val horaFin: String,

    @ColumnInfo(name = "num_personas") val numPersonas: Int

)