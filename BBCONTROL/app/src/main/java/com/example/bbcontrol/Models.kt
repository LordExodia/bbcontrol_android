package com.example.bbcontrol

import com.google.firebase.Timestamp


data class DevAlcoholicDrink(val name: String,
                             val description: String,
                             val volume: String,
                             val image : String,
                             val priceGlass: Int,
                             val priceJar: Int,
                             val pricePint: Int,
                             val priceTower: Int) {

}

data class DevNonAlcoholicDrink(val name: String,
                             val image: String,
                             val price: Int) {

}

data class DevFoodPlates(val name: String,
                         val description: String,
                         val category: String,
                         val price: Int){
}
data class DevTables(val available: Boolean,
                  val seats: Int,
                  val number: Int){
}
data class DevOrders(val idUser: String,
                     val total: Int,
                     val created: Timestamp,
                     val id : String,
                     val state : Int,
                     val waiterId: String,
                     val limit: Int,
                     val products: ArrayList<String>){
}
data class DevWaiter(val firstName : String,
                     val lastName : String,
                     val email: String,
                     val identificacion : Double,
                     val id: String,
                     val phoneNumber: Double,
                     val active: Boolean,
                     val ordersAmount: Int){

}
data class DevPromotion( val name: String,
                      val description: String,
                      val price: Int,
                      val active: Boolean,
                         val id : String){

}
data class DevReservation(val date: Timestamp,
                          val end: Timestamp,
                          val id: String,
                          val num_people: Int,
                          val preferences: ArrayList<String>,
                          val start : Timestamp,
                          val user_Id : String){
}
