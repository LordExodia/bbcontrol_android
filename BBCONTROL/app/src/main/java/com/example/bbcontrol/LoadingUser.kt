package com.example.bbcontrol

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.*
import kotlin.math.log

class LoadingUser : AppCompatActivity() {

    private lateinit var user:String
    val db = FirebaseFirestore.getInstance()
    var  newuser:String ="NOFUNCIONA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        user = intent.getStringExtra("Current")
        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("LOADING", sharedPref.all.toString())
        var loguser = sharedPref.getString("UserEmail","")
        Log.d("SP",loguser)
        if(loguser==""){
            GlobalScope.launch {
                suspend {
                    Log.d("coroutineScope", "#runs on ${Thread.currentThread().name}")
                    delay(5000)
                    withContext(Dispatchers.Main) {
                        Log.d("coroutineScope", "#runs on ${Thread.currentThread().name}")
                        getCurrentUser()
                    }
                    println("DONECORUTINES")
                }.invoke()
                Thread.sleep(2000)
                Log.d("USER",newuser)

                var part = newuser.split("[")[1].split("]")[0].split(",")
                Log.d("PARTES",part.toString())
                var userEmail = part[7]
                var userID =part[4]
                var limit = part[3].replace(" ","")

                with(sharedPref.edit()){
                    putString("UserEmail",userEmail)
                    commit()
                    putString("UserId",userID)
                    commit()
                    putString("Limit",limit)
                    commit()
                }

                println(sharedPref.all)
                val intent = Intent(this@LoadingUser, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        else if(loguser!=user){
            GlobalScope.launch {
                suspend {
                    Log.d("coroutineScope", "#runs on ${Thread.currentThread().name}")
                    delay(5000)
                    withContext(Dispatchers.Main) {
                        Log.d("coroutineScope", "#runs on ${Thread.currentThread().name}")
                        getCurrentUser()
                    }
                    println("DONECORUTINES")
                }.invoke()
                Thread.sleep(2000)
                Log.d("USER",newuser)

                var part = newuser.split("[")[1].split("]")[0].split(",")
                var userEmail = part[7]
                var userID =part[4]
                var limit = part[3].replace(" ","")

                with(sharedPref.edit()){
                    putString("UserEmail",userEmail)
                    commit()
                    putString("UserId",userID)
                    commit()
                    putString("Limit",limit)
                    commit()
                }
                println(sharedPref.all)

                val intent = Intent(this@LoadingUser, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        else if(loguser==user){
            val intent = Intent(this@LoadingUser, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }



    fun getCurrentUser() {
        val path = "/Customers"
        db.collection(path).whereEqualTo("email",user).get().addOnCompleteListener() { task: Task<QuerySnapshot> ->
            if (task.isSuccessful) {

                for (document in task.result?.documents!!) {
                    newuser = document.data?.values.toString()

                }
            }

        }

    }

}