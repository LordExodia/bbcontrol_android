package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity() {

    private var totalGlass = 0
    private var totalTower = 0
    private var totalJar = 0
    private var totalPint = 0
    var total = totalGlass + totalTower + totalJar + totalPint

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        val cache =CacheReference.getLru()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_cart)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_cart)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_cart)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_cart, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_cart)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_cart)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    val snackbar = Snackbar.make(drawer_layout_cart, "Log Out.", Snackbar.LENGTH_INDEFINITE )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok" ) {
                        snackbar.dismiss()
                    }
                    snackbar.show()
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = "Cart"
        //------------------------------------------------------------------------

        val btnComponentGlass = ButtonComponent(this)
        val btnComponentTower = ButtonComponent(this)
        val btnComponentJar = ButtonComponent(this)
        val btnComponentPint = ButtonComponent(this)

        val btnGlass = btnComponentGlass.buttonComponent()
        val btnTower = btnComponentTower.buttonComponent()
        val btnJar = btnComponentJar.buttonComponent()
        val btnPint = btnComponentPint.buttonComponent()

        llGlass.addView(btnGlass)
        llTower.addView(btnTower)
        llJar.addView(btnJar)
        llPint.addView(btnPint)

        val objectIntent: Intent = intent
        val name = objectIntent.getStringExtra("name")
        val glass = objectIntent.getIntExtra("priceGlass", 0)
        val tower = objectIntent.getIntExtra("priceTower", 0)
        val jar = objectIntent.getIntExtra("priceJar", 0)
        val pint = objectIntent.getIntExtra("pricePint", 0)

        txtGlassPrice.text = "\$ $glass"
        txtTowerPrice.text = "\$ $tower"
        txtJarPrice.text = "\$ $jar"
        txtPintPrice.text = "\$ $pint"
        txtTotal.text = "\$ $total"

        fun total() {
            totalGlass = btnComponentGlass.get().toInt() * glass
            totalTower = btnComponentTower.get().toInt() * tower
            totalJar = btnComponentJar.get().toInt() * jar
            totalPint = btnComponentPint.get().toInt() * pint
            total = totalGlass + totalTower + totalJar + totalPint

            txtTotal.text = "\$ $total"
        }



        btnComponentGlass.buttonPlus.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo =="1"){
                if(limit!=0){
                    var new = (btnComponentGlass.get().toInt()+1)*glass
                     if(tot+new >= limit){
                         val snackbar1 = Snackbar.make(drawer_layout_cart, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                         snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                         snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                         snackbar1.setAction("Ok" ) {
                             snackbar1.dismiss()
                         }
                         snackbar1.show()
                     }
                    else{
                         cache.remove("total")
                         cache.put("total", (tot+new).toString())
                         btnComponentGlass.plus()
                         total()
                     }
                }
                else{
                    btnComponentGlass.plus()
                    total()
                }
            }
            else{
                btnComponentGlass.plus()
                total()
            }

        }
        btnComponentGlass.buttonLess.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo=="1"){
                if(btnComponentGlass.get().toInt()>0){
                    var new = tot - glass
                    cache.remove("total")
                    cache.put("total", (new).toString())
                    btnComponentGlass.less()
                    total()
                }
            }
            else{
                btnComponentGlass.less()
                total()
            }
        }
        btnComponentTower.buttonPlus.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo =="1"){
                if(limit!=0){
                    var new = (btnComponentTower.get().toInt()+1)*tower
                    if(tot+new >= limit){
                        val snackbar1 = Snackbar.make(drawer_layout_cart, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                        snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                        snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                        snackbar1.setAction("Ok" ) {
                            snackbar1.dismiss()
                        }
                        snackbar1.show()
                    }
                    else{
                        cache.remove("total")
                        cache.put("total", (tot+new).toString())
                        btnComponentTower.plus()
                        total()
                    }
                }
                else{
                    btnComponentTower.plus()
                    total()
                }
            }
            else{
                btnComponentTower.plus()
                total()
            }

        }
        btnComponentTower.buttonLess.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo=="1"){
                if(btnComponentTower.get().toInt()>0){
                    var new = tot - tower
                    cache.remove("total")
                    cache.put("total", (new).toString())
                    btnComponentTower.less()
                    total()
                }
            }
            else{
                btnComponentTower.less()
                total()
            }

        }
        btnComponentJar.buttonPlus.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo =="1"){
                if(limit!=0){
                    var new = (btnComponentJar.get().toInt()+1)*jar
                    if(tot+new >= limit){
                        val snackbar1 = Snackbar.make(drawer_layout_cart, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                        snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                        snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                        snackbar1.setAction("Ok" ) {
                            snackbar1.dismiss()
                        }
                        snackbar1.show()
                    }
                    else{
                        cache.remove("total")
                        cache.put("total", (tot+new).toString())
                        btnComponentJar.plus()
                        total()
                    }
                }
                else{
                    btnComponentJar.plus()
                    total()
                }
            }
            else{
                btnComponentJar.plus()
                total()
            }
        }
        btnComponentJar.buttonLess.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo=="1"){
                if(btnComponentJar.get().toInt()>0){
                    var new = tot - jar
                    cache.remove("total")
                    cache.put("total", (new).toString())
                    btnComponentJar.less()
                    total()
                }
            }
            else{
                btnComponentJar.less()
                total()
            }

        }
        btnComponentPint.buttonPlus.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo =="1"){
                if(limit!=0){
                    var new = (btnComponentPint.get().toInt()+1)*pint
                    if(tot+new >= limit){
                        val snackbar1 = Snackbar.make(drawer_layout_cart, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                        snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                        snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                        snackbar1.setAction("Ok" ) {
                            snackbar1.dismiss()
                        }
                        snackbar1.show()
                    }
                    else{
                        cache.remove("total")
                        cache.put("total", (tot+new).toString())
                        btnComponentPint.plus()
                        total()
                    }
                }
                else{
                    btnComponentPint.plus()
                    total()
                }
            }
            else{
                btnComponentPint.plus()
                total()
            }

        }
        btnComponentPint.buttonLess.setOnClickListener {
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var tot = cache.get("total").toInt()
            if(activo=="1"){
                if(btnComponentPint.get().toInt()>0){
                    var new = tot - pint
                    cache.remove("total")
                    cache.put("total", (new).toString())
                    btnComponentPint.less()
                    total()
                }
            }
            else{
                btnComponentPint.less()
                total()
            }

        }
        btnAddOrder.setOnClickListener{
            var limit = cache.get("limit").toInt()
            var activo = cache.get("activo")
            var order =""
            if ( btnComponentGlass.get().toInt() > 0){
                val glassstr = name?.plus("-glass,").plus(glass).plus(",").plus(btnComponentGlass.get().toInt())

                order=order.plus(glassstr).plus("|")
            }
            if ( btnComponentPint.get().toInt() > 0){
                val pintstr = name?.plus("-pint,").plus(pint).plus(",").plus(btnComponentPint.get().toInt())

                order=order.plus(pintstr).plus("|")
            }
            if ( btnComponentJar.get().toInt() > 0){
                val jarstr = name?.plus("-jar,").plus(jar).plus(",").plus(btnComponentJar.get().toInt())

                order = order.plus(jarstr).plus("|")
            }
            if ( btnComponentTower.get().toInt() > 0){
                val towerstr = name?.plus("-tower,").plus(tower).plus(",").plus(btnComponentTower.get().toInt())

                order=order.plus(towerstr).plus("|")
            }
            if(cache.get("Order")==null){
                Log.d("CACHE","NO EXISTE")
                cache.put("Order",order)
            }
            else{
                Log.d("CACHE","EXISTE")
                val o = cache.get("Order")
                cache.remove("Order")
                val no = o.plus(order)
                cache.put("Order",no)
            }
            println(cache.get("Order"))

            val snackbar = Snackbar.make(drawer_layout_cart, "The items have been added to the order", Snackbar.LENGTH_INDEFINITE )

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok" ) {
                snackbar.dismiss()

                val intent = Intent(this, MenuActivity::class.java)
                startActivity(intent)
                finish()
            }
            snackbar.show()
        }

    }
}
