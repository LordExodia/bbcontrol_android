package com.example.bbcontrol

import androidx.lifecycle.LiveData

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class ReservaRepository(private val reservaDao: ReservaDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allReservas: LiveData<List<Reserva>> = reservaDao.getReservas()

    suspend fun delete(reserva: Reserva) {
        reservaDao.delete(reserva)
    }

    suspend fun insert(reserva: Reserva) {
        reservaDao.insert(reserva)
    }

    suspend fun update(reserva: Reserva) {
        reservaDao.update(reserva)
    }
}