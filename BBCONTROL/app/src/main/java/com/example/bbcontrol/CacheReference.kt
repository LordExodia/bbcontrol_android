package com.example.bbcontrol

import android.util.Log
import android.util.LruCache
import androidx.constraintlayout.solver.Cache

object CacheReference{
        private val lru: LruCache<String, String>
        fun getLru(): LruCache<String, String> {
            return lru
        }

        init {
            Log.d("LRUCACHE","START CACHE")
            lru = LruCache<String, String>(1024)
        }
}