package com.example.bbcontrol

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.admin_view.*

class AdminActivity: AppCompatActivity() {

    private var waiters = arrayListOf<DevWaiter>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_view)

        cargarMeseros()

        btnPromotions.setOnClickListener {
            val intent = Intent(this, PromotionActivity::class.java)
            finish()
            startActivity(intent)
        }

        btnAdd.setOnClickListener {
            val intent = Intent(this, CreateWaiterActivity::class.java)
            finish()
            startActivity(intent)
        }

        //------------------------------------------------------------------------
        val imageMenu = findViewById<ImageView>(R.id.imageMenu)
        imageMenu.visibility = View.INVISIBLE

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.visibility = View.INVISIBLE

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.waiter)
        //------------------------------------------------------------------------
    }

    private fun cargarMeseros(){
        val path = "BBCEmployees"

        db.collection(path)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    var cont = task.result?.documents!!.size
                    for (document in task.result?.documents!!) {
                        //println(document.data)
                        var str = ""
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str =str.plus(dat.toString()).plus("|")
                        }
                        val parts = str.split("|")
                        println(parts)
                        val waiter = DevWaiter(parts[1],parts[0], parts[7],parts[3].toDouble(),parts[5], parts[2].toDouble(),parts[4].toBoolean(), parts[6].toInt())
                        //println(waiter)
                        waiters.add(waiter)

                        if(waiters.size == cont)
                        {
                            cargar()
                        }
                    }
                    Log.d("DB","SE OBTIENE TODA LA INFORMACION DE WAITERS")
                }
            }
    }

    private fun cargar() {

        val llWaiter = findViewById<LinearLayout>(R.id.llWaiter)
        val lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        var cont = 0
        for (waiter in waiters){

            val txt1 = TextView(this)
            txt1.id = cont + 800
            txt1.layoutParams = lp2
            txt1.text = waiter.firstName.plus(" ").plus(waiter.lastName).plus("       ")
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.id = cont + 200
            txt2.layoutParams = lp2
            if (waiter.active){
                txt2.text = getString(R.string.Active)
                txt2.setTextColor(Color.GREEN)
            }
            else{
                txt2.text = getString(R.string.Inactive)
                txt2.setTextColor(Color.RED)
            }

            val chBox = CheckBox(this)
            chBox.id = cont + 400
            chBox.layoutParams = lp2
            chBox.isChecked = waiter.active

            chBox.setOnClickListener {
                var userRef = db.collection("BBCEmployees").document(waiter.id)
                userRef.update("active",chBox.isChecked)
                    .addOnSuccessListener {
                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                        val intent = Intent(this, AdminActivity::class.java)
                        finish()
                        startActivity(intent)
                    }
            }

            val card = CardView(this)
            card.id = cont + 600
            cont+=1
            card.layoutParams = lp2
            card.elevation = 10f
            card.useCompatPadding = true

            // Create RelativeLayout
            val relative = RelativeLayout(this)
            val lp: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            lp.setMargins(10,10,10,0)

            val paramsState: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsState.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsState.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsState.setMargins(10,10,0,0)

            val paramsDate: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsDate.addRule(RelativeLayout.BELOW, txt1.id)
            paramsDate.addRule(RelativeLayout.ALIGN_PARENT_START)
            paramsDate.setMargins(10,10,0,0)

            val paramsPeople: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_TOP)
            paramsPeople.addRule(RelativeLayout.ALIGN_PARENT_END)
            paramsPeople.setMargins(0,10,20,0)

            relative.addView(txt1, paramsState)
            relative.addView(txt2, paramsDate)
            relative.addView(chBox, paramsPeople)

            card.addView(relative)

            llWaiter.addView(card)
        }
    }
}