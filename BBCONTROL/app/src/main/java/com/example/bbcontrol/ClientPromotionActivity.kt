package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_promos.*

class ClientPromotionActivity: AppCompatActivity() {

    private var promotions = arrayListOf<DevPromotion>()
    val db = FirebaseFirestore.getInstance()
    private var fbtn1 : ButtonComponent? = null
    private var fbtn2 : ButtonComponent? = null
    private var fbtn3 : ButtonComponent? = null
    private var fbtn4 : ButtonComponent? = null
    private var fbtn5 : ButtonComponent? = null
    private var fbtn6 : ButtonComponent? = null
    var total = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promos)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_promos)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_promos)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_promos)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_promos, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_promos)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_promos)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_promos, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.promos)
        //------------------------------------------------------------------------

        val cache =CacheReference.getLru()

        cargarPromotions()

        btnOrder.setOnClickListener {
            var order = ""
            if(fbtn1 != null && fbtn1!!.get().toInt()>0){
                val str = promotions[0].name.plus("+f,").plus(promotions[0].price).plus(",").plus(fbtn1!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn2 != null && fbtn2!!.get().toInt()>0){
                val str = promotions[1].name.plus("+f,").plus(promotions[1].price).plus(",").plus(fbtn2!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn3 != null && fbtn3!!.get().toInt()>0){
                val str = promotions[2].name.plus("+f,").plus(promotions[2].price).plus(",").plus(fbtn3!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn4 != null && fbtn4!!.get().toInt()>0){
                val str = promotions[3].name.plus("+f,").plus(promotions[3].price).plus(",").plus(fbtn4!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn5 != null && fbtn5!!.get().toInt()>0){
                val str = promotions[4].name.plus("+f,").plus(promotions[4].price).plus(",").plus(fbtn5!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(fbtn6 != null && fbtn6!!.get().toInt()>0){
                val str = promotions[5].name.plus("+f,").plus(promotions[5].price).plus(",").plus(fbtn6!!.get().toInt())
                order=order.plus(str).plus("|")
            }
            if(cache.get("Order")==null){
                Log.d("CACHE","NO EXISTE")
                cache.put("Order",order)
            }
            else{
                Log.d("CACHE","EXISTE")
                val o = cache.get("Order")
                cache.remove("Order")
                val no = o.plus(order)
                cache.put("Order",no)
            }
            println(cache.get("Order"))
            Toast.makeText(applicationContext,"The items have been added to the order", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ClientPromotionActivity::class.java)
            startActivity(intent)
            finish()
        }
        button2.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            startActivity(intent)
            finish()
        }

    }

    private fun cargarPromotions(){
        val path = "Promotions"

        db.collection(path)
            .whereEqualTo("active",true)
            .get()
            .addOnCompleteListener { task: Task<QuerySnapshot> ->
                if (task.isSuccessful) {
                    for (document in task.result?.documents!!) {
                        var str = ""
                        val datos = Pair(document.id,document.data)
                        for (dat in datos.second?.values!!){
                            str =str.plus(dat.toString()).plus("|")
                        }
                        val parts = str.split("|")
                        println(parts)
                        if(parts[2].toString() == "true" ||parts[2].toString() == "false" ){
                            var promo = DevPromotion(
                                parts[1],
                                parts[3],
                                parts[0].toInt(),
                                parts[2].toBoolean(),
                                parts[4].toString()
                            )
                            promotions.add(promo)
                        }
                        else {
                            var promo = DevPromotion(
                                parts[1],
                                parts[2],
                                parts[0].toInt(),
                                parts[3].toBoolean(),
                                parts[4].toString()
                            )
                            promotions.add(promo)
                            //println(promo)
                        }
                    }

                    Log.d("DB","SE OBTIENE TODA LA INFORMACION DE PROMOS")
                    cargar()
                }
            }
    }

    @SuppressLint("SetTextI18n")
    private fun cargar(){
        val cache =CacheReference.getLru()
        println(promotions.size)

        textView12.text = total.toString()

        val lPromotion = findViewById<LinearLayout>(R.id.lPromos)

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val lp2 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        val len = promotions.size
        var i = 0
        for (promo in promotions){
            i += 1
            val card = CardView(this)
            card.layoutParams = lp
            card.elevation = 10f
            card.useCompatPadding = true

            val txt1 = TextView(this)
            txt1.layoutParams = lp2
            txt1.text = promo.name.plus("     ")
            txt1.textSize = 20f

            val txt2 = TextView(this)
            txt2.layoutParams = lp2
            txt2.text = "$".plus(promo.price.toString())
            txt2.setTextColor(Color.BLUE)

            val txt3 = TextView(this)
            txt3.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1.5f)
            txt3.text = promo.description

            val llayoutV = LinearLayout(this)
            llayoutV.layoutParams = lp
            llayoutV.orientation = LinearLayout.HORIZONTAL

            llayoutV.addView(txt1)
            llayoutV.addView(txt2)

            val llayoutV2 = LinearLayout(this)
            llayoutV2.layoutParams = lp
            llayoutV2.orientation = LinearLayout.HORIZONTAL

            val llayoutV3 = LinearLayout(this)
            llayoutV3.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)
            llayoutV3.orientation = LinearLayout.HORIZONTAL

            val btnComponent = ButtonComponent(this)
            val btn = btnComponent.buttonComponent()
            btn.gravity = Gravity.END
            if (len ==6) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
                if (i == 2) {
                    fbtn2 = btnComponent
                }
                if (i == 3) {
                    fbtn3 = btnComponent
                }
                if (i == 4) {
                    fbtn4 = btnComponent
                }
                if (i == 5) {
                    fbtn5 = btnComponent
                }
                if (i == 6) {
                    fbtn6 = btnComponent
                }
            }
            else if(len == 5) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
                if (i == 2) {
                    fbtn2 = btnComponent
                }
                if (i == 3) {
                    fbtn3 = btnComponent
                }
                if (i == 4) {
                    fbtn4 = btnComponent
                }
                if (i == 5) {
                    fbtn5 = btnComponent
                }
            }
            else if(len == 4) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
                if (i == 2) {
                    fbtn2 = btnComponent
                }
                if (i == 3) {
                    fbtn3 = btnComponent
                }
                if (i == 4) {
                    fbtn4 = btnComponent
                }
            }
            else if(len == 3) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
                if (i == 2) {
                    fbtn2 = btnComponent
                }
                if (i == 3) {
                    fbtn3 = btnComponent
                }
            }
            else if(len == 2) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
                if (i == 2) {
                    fbtn2 = btnComponent
                }
            }
            else if(len == 1) {
                if (i == 1) {
                    fbtn1 = btnComponent
                }
            }

            llayoutV2.addView(txt3)
            llayoutV3.addView(btn)
            llayoutV2.addView(llayoutV3)

            val llayout = LinearLayout(this)
            llayout.layoutParams = lp
            llayout.orientation = LinearLayout.VERTICAL
            llayout.addView(llayoutV)
            llayout.addView(llayoutV2)

            card.addView(llayout)

            lPromotion.addView(card)

            btnComponent.buttonPlus.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo =="1"){
                    if(limit!=0){
                        var new = (btnComponent.get().toInt()+1)*promo.price
                        if(tot+new >= limit){
                            val snackbar1 = Snackbar.make(lPromotion, "The item cannot be added since it surpasses the Expenses Limit", Snackbar.LENGTH_INDEFINITE )

                            snackbar1.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                            snackbar1.setActionTextColor(Color.parseColor("#FFFFFF"))

                            snackbar1.setAction("Ok" ) {
                                snackbar1.dismiss()
                            }
                            snackbar1.show()
                        }
                        else{
                            cache.remove("total")
                            cache.put("total", (tot+new).toString())
                            btnComponent.plus()
                            if(btnComponent.get().toInt() < 100) {
                                total += promo.price
                                textView12.text = "\$ $total"
                            }
                        }
                    }
                    else{
                        btnComponent.plus()
                        if(btnComponent.get().toInt() < 100) {
                            total += promo.price
                            textView12.text = "\$ $total"
                        }
                    }
                }
                else{
                    btnComponent.plus()
                    if(btnComponent.get().toInt() < 100) {
                        total += promo.price
                        textView12.text = "\$ $total"
                    }
                }

            }
            btnComponent.buttonLess.setOnClickListener {
                var limit = cache.get("limit").toInt()
                var activo = cache.get("activo")
                var tot = cache.get("total").toInt()
                if(activo=="1"){
                    if(btnComponent.get().toInt()>0){
                        var new = tot - promo.price
                        cache.remove("total")
                        cache.put("total", (new).toString())
                        if(btnComponent.get().toInt() != 0 && total > 0) {
                            btnComponent.less()
                            total -= promo.price
                            textView12.text = "\$ $total"
                        }
                    }
                }
                else{
                    if(btnComponent.get().toInt() != 0 && total > 0) {
                        btnComponent.less()
                        total -= promo.price
                        textView12.text = "\$ $total"
                    }
                }

            }
        }
    }
}