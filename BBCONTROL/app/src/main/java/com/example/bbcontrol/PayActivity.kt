package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_pay.*
import java.util.*
import kotlin.collections.ArrayList

class PayActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_pay)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_pay)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_pay)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_pay, // Parent view
                        "Calling the waiter.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_pay)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemControl -> {
                    val intent = Intent(this, ExpenseControlActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_pay)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    // Show a snack bar for undo option
                    val snackbar = Snackbar.make(
                        drawer_layout_pay, // Parent view
                        "Log Out.", // Message to show
                        Snackbar.LENGTH_INDEFINITE //
                    )

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction( // Set an action for snack bar
                        "Ok" // Action button text
                    ) { // Action button click listener
                        // Do something when undo action button clicked
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.pay_activity)
        //------------------------------------------------------------------------

        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("SP",sharedPref.all.toString())
        val loguser = sharedPref.getString("UserId","").toString()

        val cache = CacheReference.getLru()
        val drunkMode = cache.get("drunk")
        cargarVision(cache.get("Order"))

        btnimg1.setOnClickListener {
            if(drunkMode == "1")
                showDialog()
            else
                ordenar()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun cargarVision(orders: String){
        Log.d("CARGARVISTA","INICIANDO")
        var totalOrder =0
        val tip: Int
        val len = orders.split("|").size
        var info =""
        val items =orders.split("|")
        var i = 0
        while (i < len-1 ){
            val parts = items[i].split(",")
            val value = parts[1].toInt()*parts[2].toInt()
            info= info.plus(parts[0].split("+")[0]).plus("\t").plus(parts[2]).plus("\t").plus(value).plus("\n")
            totalOrder += value
            i+=1
        }
        Log.d("INFO",info)
        tip = (totalOrder*0.1).toInt()

        textView11.text = "".plus(info)
        textView2.text = "$".plus(totalOrder.toString())
        textView3.text = "$".plus(tip.toString())
        textView6.text = "$".plus((totalOrder+tip).toString())

    }

    private fun publicarOrden(orders: String, loguser:String){
        val cache = CacheReference.getLru()
        var limit = cache.get("limit")
        cache.remove("total")
        cache.put("total","0")
        val time = Timestamp.now()
        val id = UUID.randomUUID().toString()
        val total =textView6.text.toString().split("$")[1]
        Log.d("ID",id)
        var waiterid=""
        var waiterCant =1000000
        db.collection("BBCEmployees")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    var  str = ""
                    val datos = Pair(document.id,document.data)
                    for (dat in datos.second?.values!!){
                        str = str.plus(dat.toString()).plus(" ,")
                    }
                    val part = str.split(" ,")
                    if(part[6].toInt()<waiterCant){
                        waiterCant = part[6].toInt()
                        waiterid = part[5]
                    }
                    println(waiterid)
                }
                if (waiterid == null){waiterid=""}
                var o = orders.split("|")
                var tam = o.size
                var ar = ArrayList<String>()
                var k =0
                while (k<tam-1){
                    var a = o[k].replace(",","|")
                    ar.add(a)
                    k=k+1
                }
                val creat = hashMapOf(
                    "created" to time,
                    "id" to id,
                    "idUser" to loguser,
                    "idWaiter" to waiterid,
                    "total" to total.toInt(),
                    "limitAmount" to limit.toInt(),
                    "list" to ar,
                    "state" to 0
                )
                db.collection("Orders").document(id).set(creat)
                db.collection("BBCEmployees")
                    .whereEqualTo("id",waiterid)
                    .get()
                    .addOnSuccessListener { documents ->
                        var cantorder =0
                        for (document in documents) {
                            var str = ""
                            val datos = Pair(document.id, document.data)
                            for (dat in datos.second?.values!!) {
                                str = str.plus(dat.toString()).plus(" ,")
                            }
                            val part = str.split(" ,")
                            cantorder = part[6].toInt()+1
                        }
                        val len = orders.split("|").size
                        println(len)
                        val items =orders.split("|")
                        var i = 0
                        while (i < len-1 ) {
                            val parts = items[i].split(",")
                            var productName = parts[0].split("+")[0]
                            var beerSize = ""
                            if (productName.contains("-")) {
                                val prod = productName.split("-")
                                productName = prod[0]
                                beerSize = prod[1]
                            }
                            val price = parts[2]
                            val quantity = parts[1]
                            val prodId = UUID.randomUUID().toString()
                            val prod = hashMapOf(
                                "productName" to productName,
                                "beerSize" to beerSize,
                                "price" to price,
                                "quantity" to quantity,
                                "id" to prodId
                            )
                            db.collection("Orders/".plus(id).plus("/products")).document(prodId)
                                .set(prod).addOnSuccessListener {
                                    db.collection("BBCEmployess").document(waiterid.replace(" ",""))
                                        .update("ordersAmount",cantorder)
                                        .addOnSuccessListener {
                                            Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                                        }
                                }
                            i += 1
                        }

                    }


            }
            .addOnFailureListener { exception ->
                Log.w("WAITER", "Error getting documents: ", exception)
            }
    }

    private fun showDialog(){
        var operStr = ""
        var resultOper: Int = 0
        var result1: Int = 0
        var result2: Int = 0
        val num1 = (30..70).random()
        val num2 = (30..70).random()
        val oper = (0..1).random()

        when (oper) {
            0 -> {
                resultOper = num1 + num2
                result1 = ((resultOper-5)..(resultOper+5)).random()
                result2 = ((resultOper-5)..(resultOper+5)).random()
                operStr = "+"
            }
            1 -> {
                resultOper = num1 - num2
                result1 = ((resultOper-15)..(resultOper+15)).random()
                result2 = ((resultOper-15)..(resultOper+15)).random()
                operStr = "-"
            }
        }
        val resultados: ArrayList<Int> = ArrayList()
        resultados.add(resultOper)
        resultados.add(result1)
        resultados.add(result2)

        val alea1 = resultados.removeAt(((0 until resultados.size).random()))
        val alea2 = resultados.removeAt(((0 until resultados.size).random()))
        val alea3 = resultados.removeAt(((0 until resultados.size).random()))

        val select = getString(R.string.correct_answer)
        val textOpertion = " $num1 $operStr $num2 = "

        var dialog = AlertDialog.Builder(this)
        dialog.setCancelable(false)
        dialog.setTitle(select)
        dialog.setMessage(textOpertion)
        dialog.setPositiveButton("$alea1") { dialogs, switch ->
            if(alea1 == resultOper) {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Good answer!.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                    ordenar()
                }
                snackbar.show()
            }
            else {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.setNegativeButton("$alea2") { dialogs, switch ->
            if(alea2 == resultOper) {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Good answer!.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                    ordenar()
                }
                snackbar.show()
            }
            else {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.setNeutralButton("$alea3") { dialogs, switch ->
            if(alea3 == resultOper) {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Good answer!.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FF6DAC3B"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                    ordenar()
                }
                snackbar.show()
            }
            else {
                val snackbar = Snackbar.make(drawer_layout_pay,
                    "Wrong answer! Try Again.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#BA3535"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()
            }
        }
        dialog.show()
    }

    fun ordenar() {
        val sharedPref = getSharedPreferences("USERINFO", 0)
        Log.d("SP",sharedPref.all.toString())
        val loguser = sharedPref.getString("UserId","").toString()

        val cache = CacheReference.getLru()
        cargarVision(cache.get("Order"))

        if (cache.get("Order") == "") {
            val snackbar = Snackbar.make(
                drawer_layout_pay,
                "You cannot place an empty order",
                Snackbar.LENGTH_INDEFINITE
            )

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()
            }
            snackbar.show()
        } else {
            var online = ConnectionSingleton.isOnline(this.applicationContext)
            if (online) {
                publicarOrden(cache.get("Order"), loguser)

                cache.remove("Order")

                val snackbar = Snackbar.make(
                    drawer_layout_pay,
                    "Order place successfully.",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()

                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                snackbar.show()

            } else {
                val snackbar = Snackbar.make(
                    drawer_layout_pay,
                    "Couldn't place order. \n Check your internet connection",
                    Snackbar.LENGTH_INDEFINITE
                )

                snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                snackbar.setAction("Ok") {
                    snackbar.dismiss()
                }
                snackbar.show()


            }
        }
    }
}
