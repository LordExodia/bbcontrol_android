package com.example.bbcontrol.ui.login

import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.core.content.ContextCompat.startActivity
import com.example.bbcontrol.MainActivity
import com.example.bbcontrol.data.LoginRepository

import com.example.bbcontrol.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private lateinit var auth: FirebaseAuth

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    var resp = false


    fun login(user: FirebaseUser) {
        // can be launched in a separate asynchronous job

        if (user != null) {
            Log.d("TAG", user?.email.toString())
            _loginResult.value =
                 LoginResult(success = LoggedInUserView(displayName = user.email.toString()))
           } else {
               // If sign in fails, display a message to the user.
               Log.w("TAG", "LoginUserEmail:failure")
               _loginResult.value = LoginResult(error = R.string.login_failed)
               Log.d("TAG", loginResult.value.toString())

           }

    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }



}
