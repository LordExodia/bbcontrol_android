package com.example.bbcontrol

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.bbcontrol.ui.login.LoginActivity
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_expense_control.*

class ExpenseControlActivity : AppCompatActivity() {

    var current_limit = 0
    val db = FirebaseFirestore.getInstance()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expense_control)

        //------------------------------------------------------------------------
        findViewById<ImageView>(R.id.imageMenu).setOnClickListener{
            val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_control)
            drawerLayout.openDrawer(GravityCompat.END)
        }

        val imageBack = findViewById<ImageView>(R.id.imageBack)
        imageBack.setOnClickListener {
            finish()
        }

        val navigationView = findViewById<NavigationView>(R.id.navigationView)
        navigationView.setNavigationItemSelectedListener {
            // Handle item selection
            when (it.itemId) {
                R.id.itemProfile -> {
                    finish()
                    val intent = Intent(this, ProfileActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_control)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemOrder -> {
                    finish()
                    val intent = Intent(this, OrderActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_control)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemWaiter -> {
                    val snackbar = Snackbar.make(drawer_layout_control,"Calling the waiter.",Snackbar.LENGTH_INDEFINITE)

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok") {
                        snackbar.dismiss()
                    }
                    snackbar.show() // Finally show the snack bar

                }
                R.id.itemDrunkMode -> {
                    finish()
                    val intent = Intent(this, DrunkModeActivity::class.java)
                    val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout_control)
                    drawerLayout.closeDrawer(GravityCompat.END)
                    startActivity(intent)
                }
                R.id.itemLogOut -> {

                    startActivity(
                        Intent(baseContext, LoginActivity::class.java)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                    )
                    finish()

                    val snackbar = Snackbar.make(drawer_layout_control, "Log Out.", Snackbar.LENGTH_INDEFINITE)

                    snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
                    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

                    snackbar.setAction("Ok") {
                        snackbar.dismiss()
                    }
                    snackbar.show()
                }
            }
            true
        }

        val title = findViewById<TextView>(R.id.textTile)
        title.text = getString(R.string.control_activity)
        //------------------------------------------------------------------------
        val cache = CacheReference.getLru()
        val txtCurrentLimit = findViewById<TextView>(R.id.currentLimit)
        txtCurrentLimit.text = "Current limit amount: \$" + cache.get("limit")

        val checkSetLimit = findViewById<CheckBox>(R.id.limit)
        if(cache.get("activo") =="1"){
            checkSetLimit.isChecked=true
        }

        set_limit.setOnClickListener {
            var message = ""
            val cache = CacheReference.getLru()
            val sharedPref = getSharedPreferences("USERINFO", 0)
            Log.d("SP",sharedPref.all.toString())
            val loguser = sharedPref.getString("UserId","").toString().replace(" ","")
            if (checkSetLimit.isChecked){


                cache.remove("limit")
                cache.remove("activo")
                message = "Expenses control activated"
                /*** Fijar el limite global, se podria usar la variable current_limit ***/
                val txtAmount = findViewById<TextView>(R.id.amount)
                cache.put("limit",txtAmount.text.toString())
                cache.put("activo","1")
                Log.d("CACHE", cache.get("activo"))
                Log.d("CACHE", cache.get("limit"))
                var userRef = db.collection("Customers").document(loguser)
                userRef.update("limitAmount",txtAmount.text.toString().toInt())
                    .addOnSuccessListener {
                        Log.d("UPDATE", "DocumentSnapshot successfully updated!")
                    }
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            else {
                message = "Expenses control disabled"
                cache.remove("activo")
                cache.put("activo","0")
                Log.d("CACHE", cache.get("activo"))
                Log.d("CACHE", cache.get("limit"))
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                /*** Quitar el limite ***/

            }

            val snackbar = Snackbar.make(drawer_layout_control, message, Snackbar.LENGTH_INDEFINITE)

            snackbar.view.setBackgroundColor(Color.parseColor("#FFB75BA4"))
            snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))

            snackbar.setAction("Ok") {
                snackbar.dismiss()
            }
            snackbar.show()
        }

    }
}